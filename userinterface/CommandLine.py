from tradingplan import *


def getUserSpecifiedTradingPlan():
    inputToTradingPlan = {
        "buy and hold": BuyAndHold(),
        "moving average": MovingAverage(),
        "random forest": RandomForest(),
        "neural network": NeuralNetwork(),
        "lstm network": LstmNetwork(),
        "wavelet autoencoder lstm": WaveletAutoEncoderLstm(),
        "naive risk reward balancer": NaiveRiskRewardBalancer()
    }
    print("\nTrading Plans:\n\t", end="")
    for key in inputToTradingPlan.keys():
        print("\"", key, "\"", sep="", end="\t")
    userInput = input("\nPlease input the trading plan for the backtest: ")
    tradingPlan = inputToTradingPlan[userInput]
    return tradingPlan


def getValidUserSpecifiedTradingPlan():
    tradingPlan = None
    while tradingPlan is None:
        try:
            tradingPlan = getUserSpecifiedTradingPlan()
        except KeyError:
            print("Invalid input")
    return tradingPlan

