from enum import Enum
from abc import abstractmethod
from util import *


class OrderStatus(Enum):
    OPEN = 0
    FILLED = 1
    FAILED = -1


class Order:
    """
    Base class for stock market orders to initiate variables. Cannot be instantiated directly.
    """

    def __init__(self, stockExchange, investor, tickerSymbol, date, units):
        if type(self) is Order:
            raise TypeError("Base class may not be instantiated")

        self.stockExchange = stockExchange
        self.tickerSymbol = tickerSymbol
        self.stock = investor.stockExchange.getStockByTickerSymbol(self.tickerSymbol)
        self.index = self.stockExchange.getIndexByTickerSymbol(self.tickerSymbol)
        self.investor = investor
        self.date = date
        self.units = units
        self.status = OrderStatus.OPEN

    @abstractmethod
    def execute(self, date):
        """
        Public method for StockExchange to execute the order on the specified date.
        This method must be overridden by the subclass.

        :param date:
        :return:
        """
        pass


class BuyOrder(Order):
    """
    A limit buy order.
    """

    def __init__(self, stockExchange, investor, tickerSymbol, date, units, limitPrice, cashOnHold):
        super().__init__(stockExchange, investor, tickerSymbol, date, units)
        self.limitPrice = limitPrice
        self.cashOnHold = cashOnHold

    @overrides(Order)
    def execute(self, date):
        df = self.stock.df
        if self.limitPrice >= df.loc[date]['low']:
            self.investor.portfolio.changeUnits(self.index, self.units, date)
            self.investor.portfolio.changeAvailableUnits(self.index, self.units, date)

            self.investor.unitsBought += self.units
            self.investor.buys += 1
            self.status = OrderStatus.FILLED
            print("buy order filled:\t", self.units, "units at $", self.limitPrice)
        else:
            print("BUY ORDER FAILED, buying at $", self.limitPrice, ", lowest market price $", df.loc[date]['low'])
            # order failed - refund money
            self.investor.portfolio.changeCash(+self.cashOnHold, date)
            self.investor.failedBuys += 1
            self.status = OrderStatus.FAILED


class MidPointBuyOrder(Order):
    """
    A buy order which assumes midpoint price execution on the next trading day.
    Maximum units are decided on execution based on the cash placed in the order.
    """

    def __init__(self, stockExchange, investor, tickerSymbol, date, cashOnHold, valueHolder=None):
        super().__init__(stockExchange, investor, tickerSymbol, date, units=None)
        self.cashOnHold = cashOnHold
        self.valueHolder = valueHolder

    @overrides(Order)
    def execute(self, date):
        df = self.stock.df
        maxBuyUnits = self._maxBuyUnits(date)
        if maxBuyUnits > 0:
            self.cashOnHold -= maxBuyUnits * df.loc[date]['avg']
            assert (self.cashOnHold >= 0)
            self.investor.portfolio.changeUnits(self.index, maxBuyUnits, date)
            self.investor.portfolio.changeAvailableUnits(self.index, maxBuyUnits, date)
            self.investor.portfolio.changeCash(self.cashOnHold, date)

            self.investor.unitsBought += maxBuyUnits
            self.investor.buys += 1
            self.status = OrderStatus.FILLED
            if self.valueHolder is not None:
                self.valueHolder.value = df.loc[date]['avg']
            print("Mid-point Buy Order filled:\t", maxBuyUnits, "units at $", df.loc[date]['avg'])
        else:
            print("BUY ORDER FAILED, $", self.cashOnHold,
                  " is insufficient to buy at least 1 share at market price $", df.loc[date]['avg'])
            # order failed - refund money
            self.investor.portfolio.changeCash(+self.cashOnHold, date)
            self.investor.failedBuys += 1
            self.status = OrderStatus.FAILED

    def _maxBuyUnits(self, _date):
        return int(self.cashOnHold / self.stock.df.loc[_date]['avg'])


class SellOrder(Order):
    """
    A limit sell order.
    """

    def __init__(self, stockExchange, investor, tickerSymbol, date, units, limitPrice, tradingFee):
        super().__init__(stockExchange, investor, tickerSymbol, date, units)
        self.limitPrice = limitPrice
        self.tradingFee = tradingFee

    @overrides(Order)
    def execute(self, date):
        df = self.stock.df
        if self.limitPrice <= df.loc[date]['high']:
            self.investor.portfolio.changeCash(self.limitPrice * self.units, date)
            self.investor.portfolio.changeUnits(self.index, -self.units, date)

            self.investor.unitsSold += self.units
            self.investor.sells += 1
            self.status = OrderStatus.FILLED
            print("sell order filled:\t", self.units, "units at $", self.limitPrice)
        else:
            print("SELL ORDER FAILED")
            # refund trading fee
            self.investor.portfolio.changeCash(+self.tradingFee, date)
            self.investor.portfolio.changeAvailableUnits(self.index, self.units, date)
            self.investor.failedSells += 1
            self.status = OrderStatus.FAILED


# update stop loss order to continue for a specified number of trading days
class StopLossOrder(Order):
    """
    A stop loss limit order which is in affect for the next trading day only.
    """

    def __init__(self, stockExchange, investor, tickerSymbol, date, units, limitPrice, tradingFee):
        super().__init__(stockExchange, investor, tickerSymbol, date, units)
        self.limitPrice = limitPrice
        self.tradingFee = tradingFee

    @overrides(Order)
    def execute(self, date):
        df = self.stock.df
        if (df.loc[date]['low'] <= self.limitPrice) and (df.loc[date]['high'] >= self.limitPrice):
            print("Stop loss order filled:\t", self.units, "units at $", self.limitPrice)
            self.investor.portfolio.changeCash(self.limitPrice * self.units, date)
            self.investor.portfolio.changeUnits(self.index, -self.units, date)

            self.investor.unitsSold += self.units
            self.investor.sells += 1
            self.status = OrderStatus.FILLED
        else:
            # refund trading fee
            self.investor.portfolio.changeCash(+self.tradingFee, date)
            self.investor.portfolio.changeAvailableUnits(self.index, self.units, date)
            self.status = OrderStatus.FAILED
