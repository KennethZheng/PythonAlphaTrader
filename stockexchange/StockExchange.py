import stockexchange.Order as od
from dataframe.DataFrameFactory import DataFrameFactory


class StockExchange:
    """
    Provides an aggregated collection of stock's historical data in which an investor's orders are executed.
    """

    def __init__(self, name, stockList):
        self.name = name
        self.stockList = stockList
        self.stockCount = len(self.stockList)
        self.orders = list()

        self.stockLength = None
        self.firstIndex = None
        self.lastIndex = None
        self._syncStockTimePeriod()
        assert (self.stockLength is not None)
        assert (self.firstIndex is not None)
        assert (self.lastIndex is not None)

        self.tickerSymbols = list()
        for stock in self.stockList:
            self.tickerSymbols.append(stock.tickerSymbol)

        self.tickerToStock = dict()
        for stock in self.stockList:
            self.tickerToStock.update({stock.tickerSymbol: stock})

        self.tickerToIndex = dict()
        for i in range(self.stockCount):
            self.tickerToIndex.update({self.getStockByIndex(i).tickerSymbol: i})

    def getStockByIndex(self, index):
        """
        Get a stock by its StockExchange index.

        :param index: StockExchange index
        :return:
        """
        return self.stockList[index]

    def getStockByTickerSymbol(self, tickerSymbol):
        """
        Get a stock by its tickerSymbol.

        :param tickerSymbol:
        :return:
        """
        return self.tickerToStock.get(tickerSymbol)

    def getIndexByTickerSymbol(self, tickerSymbol):
        """
        Get a stock's index by its tickerSymbol. This index is applicable to the Investor's
        StockExchange only.

        :param tickerSymbol:
        :return:
        """
        return self.tickerToIndex.get(tickerSymbol)

    def sendBuyOrder(self, investor, tickerSymbol, date, units, limitPrice, cashOnHold):
        """
        API for Investor to send a buy limit order to StockExchange which then creates the actual Order instance.

        :param investor:
        :param tickerSymbol:   Stock ticker symbol
        :param date:
        :param units:
        :param limitPrice:
        :param cashOnHold:     Investor puts down cash for buy order
        :return:
        """
        order = od.BuyOrder(self, investor, tickerSymbol, date, units, limitPrice, cashOnHold)
        self.orders.append(order)

    def sendMidPointBuyOrder(self, investor, tickerSymbol, date, cashOnHold, valueHolder=None):
        """
        API for Investor to send a mid point buy order to StockExchange which then creates the actual Order instance.

        :param investor:
        :param tickerSymbol:
        :param date:
        :param cashOnHold:
        :param valueHolder:     Optional - stores the price the order is executed at in the specified ValueHolder
        :return:
        """

        order = od.MidPointBuyOrder(self, investor, tickerSymbol, date, cashOnHold, valueHolder=valueHolder)
        self.orders.append(order)

    def sendSellOrder(self, investor, tickerSymbol, date, units, limitPrice, tradingFee):
        """
        API for Investor to send a sell limit order to StockExchange which then creates the actual Order instance.

        :param investor:
        :param tickerSymbol:   Stock ticker symbol
        :param date:
        :param units:
        :param limitPrice:
        :param tradingFee:     Investor puts down cash for executing order
        :return:
        """
        order = od.SellOrder(self, investor, tickerSymbol, date, units, limitPrice, tradingFee)
        self.orders.append(order)

    def sendStopLossOrder(self, investor, tickerSymbol, date, units, limitPrice, tradingFee):
        """
        API for Investor to send a stop limit order to StockExchange which then creates the actual Order instance.
        The order lasts for a duration of 1 day

        :param investor:
        :param tickerSymbol:   Stock ticker symbol
        :param date:
        :param units:
        :param limitPrice:
        :param tradingFee:     Investor puts down cash for executing order
        :return:
        """
        order = od.StopLossOrder(self, investor, tickerSymbol, date, units, limitPrice, tradingFee)
        self.orders.append(order)

    def printInfo(self):
        print("=======================")
        print("Stock Exchange:", self.name)
        print("=======================")
        for i in self.stockList:
            print("\t", i.tickerSymbol)

    def setDate(self, date):
        # debug
        print("SE date", date, "\t$", self.getStockByIndex(0).df.loc[date]['avg'])

        # go through orders list
        for order in self.orders:
            order.execute(date)
            self.orders.remove(order)

    def _syncStockTimePeriod(self):
        assert (len(self.stockList) > 0)
        minStockLength = -1
        for stock in self.stockList:
            if ((minStockLength == -1)
                    | (stock.getLength().value < minStockLength)):
                minStockLength = stock.getLength().value
        assert (minStockLength > 0)

        self.stockLength = minStockLength
        self.firstIndex = 0
        self.lastIndex = self.stockLength - 1
        assert(self.lastIndex == self.getStockByIndex(0).getLastIndex().value)

        for stock in self.stockList:
            stock.df = DataFrameFactory.reduceTimePeriod(stock.df, minStockLength)
