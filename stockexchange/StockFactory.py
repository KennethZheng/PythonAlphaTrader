from dataframe.EconomicData import EconomicData
from stockexchange.Stock import Stock


def loadStockSP00():
    """
    Create a stock object containing market data for the S&P 500 index. Integrate economic data into the object.
    The returned stock object can be used for back-testing.

    :return: sp500
    """
    sp500 = Stock("SP500", 'marketdata/SP500.csv', remove=4000+8485-3000)
    econ_aaa_bonds = EconomicData("Moody's Seasoned Aaa Corporate Bond Yield", 'AAA', 'marketdata/DAAA.csv',
                                  sp500.df[['date']], dataColumn='daaa')
    econ_libor = EconomicData("1-Month London Interbank Offered Rate (LIBOR), based on U.S. Dollar", 'LIBOR',
                              'marketdata/USD1MTD156N.csv', sp500.df[['date']], dataColumn='usd1mtd156n')
    econ_tbill = EconomicData("3-Month Treasury Bill: Secondary Market Rate (DTB3)", 'TB3',
                              'marketdata/DTB3.csv', sp500.df[['date']], dataColumn='dtb3')

    sp500.integrateEconomicData(econ_aaa_bonds)
    sp500.integrateEconomicData(econ_libor)
    sp500.integrateEconomicData(econ_tbill)
    return sp500


def loadStockVFV():
    """
    Create a stock object containing market data for TSE: VFV.

    :return:
    """
    return Stock("VFV.TO", 'marketdata/VFV.TO.csv', remove=400)

