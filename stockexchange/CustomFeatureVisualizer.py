from stockexchange.StockFeatureVisualizer import DataFrameFeatureVisualizer, StockFeatureVisualizer
from util import YEAR


def visualizeData(stock):
    """
    Custom function to visualize specific stock features.

    :param stock:   stock object we are visualizing
    :return:
    """
    stockVisualizer = StockFeatureVisualizer(stock, figSizeTuple=(50, 15)).printFeatureListStats()

    # momentum7
    stockVisualizer.plotFeatures(['momentum7'])

    # momentum7 sigmoid
    stockVisualizer.plotFeatures(['sigmoid_momentum7'])

    # high/low bands
    stockVisualizer.plotFeatures(['avg', 'sma100', 'sma200'])

    stockVisualizer.setFigSize(figSizeTuple=(50, 10))
    stockVisualizer.plotFeatures(['newHigh100Sma21', 'newLow100Sma21', 'trend100', 'trend100Sma100'])

    # money flow index
    stockVisualizer.plotFeatures(['percent_cmfv200'])

    # macd
    stockVisualizer.plotFeatures(['percent_macd50-200'])

    # volume
    stockVisualizer.plotFeatures(['percent_volumeE14-200'])
    stockVisualizer.plotFeatures(['percent_volumeE14-50'])

    # recent sp500
    timePeriod = YEAR*4
    recentStockDf = stock.getTimePeriod(
        stock.getLastIndex().value-timePeriod, stock.getLastIndex().value)

    recentStockVisualizer = DataFrameFeatureVisualizer(recentStockDf, (100, 10))
    recentStockVisualizer.plotFeatures(['close', 'sma100'])

    recentStockVisualizer.setFigSize(figSizeTuple=(20, 10))
    recentStockVisualizer.plotFeatures(['posChange', 'negChange'])
    recentStockVisualizer.plotFeatures(['rsi50'])
    recentStockVisualizer.plotFeatures(['percent_dailyChange'])
    recentStockVisualizer.plotFeatures(['percent_cdailyChange7'])

