from dataframe.DataFrameExtractor import *
from dataframe.DataFrameBuilder import *
from dataframe.DataFrameFactory import *
from util import *


class Stock(DataFrameExtractor, DataFrameBuilder):
    def __init__(self, _tickerSymbol, _csvFilePath, remove=200):
        # construct dataframe
        self.df = DataFrameFactory.createDataFrame(_csvFilePath)
        DataFrameBuilder.__init__(self, self.df)
        self.setFeatureColumns()
        self.df = DataFrameFactory.finalizeDataFrame(self.df, remove)

        DataFrameExtractor.__init__(self, self.df)
        self.tickerSymbol = _tickerSymbol
        print("stock df shape", self.df.shape)
        self.externalFeatures = list()

    def getQuoteByDate(self, dateIndex):
        return self.df.loc[dateIndex][self.df.columns]

    def integrateEconomicData(self, economicData, minLength=YEAR):
        econDataFrame = economicData.df
        assert (len(econDataFrame.index) >= minLength), "economic dataframe size too small"
        self.df = pd.merge(self.df, econDataFrame, on='date', how='left')
        DataFrameFactory.assertNotContainingNull(self.df)
        self.externalFeatures = self.externalFeatures + economicData.features
