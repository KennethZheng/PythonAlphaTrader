import matplotlib.pyplot as plt


class DataFrameFeatureVisualizer:
    """
    Allows visualization of the features present in the underlying dataframe of a stock.
    """

    def __init__(self, df, figSizeTuple=(50, 10)):
        self.df = df
        self.figSize = figSizeTuple

    def setFigSize(self, figSizeTuple):
        self.figSize = figSizeTuple
        return self

    def plotFeatures(self, columnList):
        plt.figure(figsize=self.figSize)
        plt.plot(self.df['date'], self.df[columnList])
        plt.title("Feature Columns: "+str(columnList))
        plt.show()
        return self

    def printFeatureStats(self, columnName):
        print(columnName, "\tmax:", round(self.df[columnName].max(), 3), "\tmin:", round(self.df[columnName].min(), 3),
              "\trange", abs(round(self.df[columnName].max() - self.df[columnName].min(), 3)),
              "\tmean:", round(self.df[columnName].mean(), 3))

    def printFeatureHistogram(self, columnName):
        plt.figure(figsize=self.figSize)
        plt.hist(self.df[columnName].values, bins='auto')
        plt.title("Feature Column: "+str(columnName))
        plt.show()
        return self

    def printFeatureListStats(self, features, showHist=False):
        for feature in features:
            self.printFeatureStats(feature)
            if showHist is True:
                self.printFeatureHistogram(feature)
        return self

    def printFeaturesByThreshold(self, columnName, threshold=0, greaterThan=True):
        if greaterThan is True:
            valuesGreaterThan = len(self.df[self.df[columnName] >= threshold])
            print("percentage of", columnName, "values greater than or equal", threshold, ":",
                  100*valuesGreaterThan/len(self.df), "%")
        else:
            valuesLessThan = len(self.df[self.df[columnName] <= threshold])
            print("percentage of", columnName, "values less than or equal", threshold, ":",
                  100*valuesLessThan/len(self.df), "%")
        return self

    def isDistributionNormal(self, columnName):
        pass

    def getPairWiseCorrelationCoefficient(self, column1, column2):
        pass


class StockFeatureVisualizer(DataFrameFeatureVisualizer):
    def __init__(self, stock, figSizeTuple=(50, 10)):
        self.stock = stock
        self.dataFrameVisualizer = super().__init__(self.stock.df, figSizeTuple)

    def printFeatureList(self):
        print(self.stock.tickerSymbol, "features:", self.stock.features)
        return self

    def printFeatureListStats(self, features=None, showHist=False):
        if features is None:
            self.printFeatureListStats(self.stock.features)
        else:
            super().printFeatureListStats(features)
        return self
