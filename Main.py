from backtest.CustomBackTester import runCustomBackTestNYSE
from stockexchange.StockFactory import loadStockSP00, loadStockVFV
from stockexchange.CustomFeatureVisualizer import visualizeData
from stockexchange.StockExchange import *
from userinterface.CommandLine import getValidUserSpecifiedTradingPlan
from util import *


def main():
    """
    Loads historical csv market data and instantiates data structures for it. Takes user input to select a trading
    plan to backtest against the benchmark buy and hold strategy.
    :return:
    """

    print("==========================")
    print("Python Alpha Trader")
    print("==========================")
    print("loading market data...")

    # create stocks
    stockSP500 = loadStockSP00()
    stockVFV = loadStockVFV()

    # create stock exchanges
    NYSE = StockExchange("NYSE", [stockSP500])
    TSX = StockExchange("TSX", [stockVFV])

    print("\nmarket data has been successfully loaded\n")

    printBenchmarkStats(0, len(stockSP500.df)-1, stockSP500)

    visualizeData(stockSP500)

    # ===== Back Testing =====
    runCustomBackTestNYSE(NYSE, tradingPlan=getValidUserSpecifiedTradingPlan())


# market benchmark stats
def printBenchmarkStats(startDate, endDate, stock):
    years = stock.getLength().value/YEAR
    stock_annual_return = (stock.df.loc[endDate]['avg']/stock.df.loc[startDate]['avg'])**(1/years)
    print("=======", stock.tickerSymbol, "Buy and Hold Benchmark =======")
    print("\tAverage annual return (market value): % 6.3f" % ((stock_annual_return-1)*100), "%")
    print("\tover% 6.1f" % years, "years")


if __name__ == '__main__':
    main()
