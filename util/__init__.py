from util.Constant import *
from util.Override import *


def isNumber(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


class ValueHolder:
    """
    Allows a value to be passed from one class to another in a delayed manner.
    """

    def __init__(self, dataType=None):
        self.dataType = dataType
        self.value = None
        self.valueSet = False

    @property
    def value(self):
        if self.dataType is not None:
            assert(self.valueSet is True)
        return self.__value

    @value.setter
    def value(self, value):
        if self.dataType is not None:
            assert(type(value) is self.dataType)
        self.__value = value
        self.valueSet = True
