"""
overrides decorator used to document and verify that the method from the superclass is overridden.
This validation occurs during method creation and therefore is irrelevant to runtime efficiency.
"""


def overrides(superClass):
    def overrider(method):
        assert(method.__name__ in dir(superClass))
        return method
    return overrider
