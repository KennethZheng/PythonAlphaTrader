# Python Alpha Trader
created by Kenneth Zheng

A personal project using python, machine learning, and historical stock market data to train and backtest various
trading models/strategies. 

## Pre-requisites

Here is a list of the external libraries used:

- abc
- keras
- matplotlib
- numpy
- pandas
- pywt
- seaborn
- sklearn
- tensorflow

## Technologies Used

* [Python3](https://www.python.org/downloads/) - Version 3.6
* [Anaconda](https://www.anaconda.com/download/) - Python environment and Jupyter Notebook
* [Pycharm](https://www.jetbrains.com/pycharm/) - The IDE used

## Implementation Overview

### Market Data
Historical market data spanning decades which includes the intra-day movements is difficult 
to obtain or requires purchasing it from a data vendor. Thus, I used freely available .csv files of the day to day 
OHLCV (open, high, low, close, volume) which can be downloaded from Yahoo Finance. A data structure, Stock, is used to 
store information of a particular ticker symbol, including the historical data stored as a pandas dataframe, as well as 
methods to access various bits of information. One of its base classes, DataFrameBuilder, extends the dataframe by 
adding feature columns of technical indicators such as rsi, macd, moving averages, variance, etc. Similarly a shared 
data structure, EconomicData, stores various economic indicators such as corporate bond yields, London Interbank 
Offered Rate (LIBOR), and treasury bill yields. This data can be integrated into the Stock dataframe via its public 
method and is implemented as a pandas dataframe merge with additional null checks.
