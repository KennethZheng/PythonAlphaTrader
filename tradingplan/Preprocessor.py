import numpy as np


class Preprocessor:
    """
    Utility methods for trading plans to pre-process or integrate data before applying machine learning algorithms.
    """

    def __init__(self):
        pass

    @staticmethod
    def createTrainingBackTestingDataFrames(stock, earliestDate, dateStart, latestDate, validationDataRatio):
        # create training and backtest dataframe
        backTestDateStart = dateStart
        assert (backTestDateStart >= earliestDate)
        assert (backTestDateStart < latestDate)

        backTestDateEnd = latestDate
        assert (backTestDateEnd > backTestDateStart)
        assert (backTestDateEnd <= latestDate)

        backTestPeriodStock = stock.getTimePeriod(backTestDateStart, backTestDateEnd).copy(deep=True)

        # reduce selected stock to training time period
        trainPeriodStock = stock.getTimePeriod(earliestDate, backTestDateStart - 1).copy(deep=True)

        return trainPeriodStock, backTestPeriodStock

    @staticmethod
    def scaleDataFrameByTanhEstimators(columns, dataframe, unscaledColumns=None):
        Mean = dataframe[columns].mean()
        StandardDeviation = dataframe[columns].std()
        TanhInput = 0.01 * (dataframe[columns] - Mean) / StandardDeviation
        TanhNdArray = np.tanh(TanhInput)
        # dataframe[columns] = 0.5 * (TanhNdArray + 1)
        dataframe[columns] = TanhNdArray
        return dataframe[columns + unscaledColumns]

    @staticmethod
    def scaleDataFrameByMeanAbsDev(columns, dataframe):
        # scale each columns by its mean absolute deviation
        AbsoluteDeviation = dataframe[columns].abs()
        MeanAbsoluteDeviation = AbsoluteDeviation.sum(axis=0) / dataframe.shape[0]
        dataframe[columns] = dataframe[columns] / MeanAbsoluteDeviation
        return dataframe[columns], MeanAbsoluteDeviation

    @staticmethod
    def rescaleDataFrameFromMeanAbsDev(dataframe, meanAbsDevDataframe):
        rescaledDataframe = dataframe * meanAbsDevDataframe
        return rescaledDataframe

    @staticmethod
    def scaleSeriesDataFromMeanAbsDev(series, meanAbsDevDataframe):
        scaledSeriesData = series / meanAbsDevDataframe
        return scaledSeriesData

    @staticmethod
    def rescaleSeriesDataFromMeanAbsDev(series, meanAbsDevDataframe, columnName):
        series = series * meanAbsDevDataframe.loc[columnName]
        return series

    @staticmethod
    def emphasizeColumn(dataframe, column, emphasisMultiplier):
        dataframe[column] = dataframe[column] * emphasisMultiplier
        return dataframe

    @staticmethod
    def deEmphasizeColumn(dataframe, column, emphasisMultiplier):
        dataframe[column] = dataframe[column] / emphasisMultiplier
        return dataframe

    @staticmethod
    def emphasizeSeries(series, emphasisMultiplier):
        series = series * emphasisMultiplier
        return series

    @staticmethod
    def deEmphasizeSeries(series, emphasisMultiplier):
        series = series / emphasisMultiplier
        return series
