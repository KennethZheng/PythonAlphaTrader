from sklearn.metrics import mean_absolute_error
from sklearn.neural_network import MLPRegressor

from tradingplan.Preprocessor import *
from tradingplan.TradingPlan import *
from sklearn.model_selection import train_test_split


# ! Under-performs market
class NeuralNetwork(TradingPlan):
    """
    Trading plan using a deep feed forward neural network to avoid market declines by more than 5% in a 50 day window.
    """

    def __init__(self):
        super().__init__()
        self.trainPeriodStock = None
        self.trainPeriodStockMeanAbsDev = None
        self.backTestPeriodStock = None
        self.backTestPeriodStockMeanAbsDev = None
        self.backTestDateStart = None
        self.backTestDateEnd = None
        self.predictDaysAhead = 50
        self.predictionColumn = 'future_priceChange' + str(self.predictDaysAhead)
        self.neuralNetwork = None
        self.indicators = [
            'variance14',
            'volumeWeightedPercentSma14', 'cci14', 'avgTrueRange14', 'momentum14', 'priceROC14', 'variance21',
            'volumeWeightedPercentSma21', 'cci21', 'avgTrueRange21', 'momentum21', 'priceROC21', 'variance50',
            'volumeWeightedPercentSma50', 'cci50', 'avgTrueRange50', 'momentum50', 'priceROC50', 'variance100',
            'volumeWeightedPercentSma100', 'cci100', 'avgTrueRange100', 'momentum100', 'priceROC100', 'variance200',
            'volumeWeightedPercentSma200', 'cci200', 'avgTrueRange200', 'momentum200', 'priceROC200', 'smaRatio7/14',
            'smaRatio14/21', 'smaRatio21/50', 'smaRatio50/100', 'smaRatio100/200', 'percent_volumeE14-21',
            'percent_volumeE14-200', 'percent_volumeE14-50', 'percent_volume14-50', 'percent_volume14-200',
            'percent_volume21-50', 'percent_volume50-100', 'percent_volume50-200', 'percent_macd14-21',
            'percent_macd14-50', 'percent_macd21-50', 'percent_macd50-100', 'percent_macd50-200',
            'percent_cmfv14', 'rsi14', 'percent_cmfv21', 'rsi21', 'percent_cmfv50', 'rsi50', 'percent_cmfv100',
            'rsi100', 'percent_cmfv200', 'rsi200', 'percent_cdailyChange7', 'smi14',
            'percent_extreme14', 'percent_cdailyChange14', 'smi21', 'percent_extreme21', 'percent_cdailyChange21',
            'smi50', 'percent_extreme50', 'percent_cdailyChange50', 'smi100', 'percent_extreme100',
            'percent_cdailyChange100', 'smi200', 'percent_extreme200', 'percent_cdailyChange200', 'trend14', 'trend21',
            'trend50', 'trend100', 'trend200', 'trend14Sma14', 'trend21Sma21', 'trend50Sma50', 'trend100Sma100']
        self.relevantColumns = None
        self.relevantColumnsCount = None

    def evaluateModel(self, neuralNetwork, X_train, y_train, X_test, y_test):
        predictions = neuralNetwork.predict(X_test)
        # print("Neural network score: ", mlp.score(X_test, y_test))
        print("Neural network mean abs error: ", mean_absolute_error(predictions, y_test))

        X_train2, X_test2, y_train2, y_test2 = train_test_split(
            self.backTestPeriodStock[self.indicators], self.backTestPeriodStock[self.predictionColumn],
            test_size=0.99, random_state=42)

        backTestPeriodPredictions = neuralNetwork.predict(X_test2)
        # print("Neural network backtest score: ", mlp.score(X_test2, y_test2))
        print("Neural network backtest mean abs error: ", mean_absolute_error(backTestPeriodPredictions, y_test2))
        # print(backTestPeriodPredictions)
        print("2* standard dev of predictions", 2 * np.std(y_test2, axis=0))

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        self.backTestDateStart = dateStart
        self.backTestDateEnd = latestDate
        self.relevantColumns = self.indicators + [self.predictionColumn]
        self.relevantColumnsCount = len(self.relevantColumns)

        stock = client.stockExchange.getStockByIndex(0)

        # partition stock for training and backtest
        self.trainPeriodStock, self.backTestPeriodStock = Preprocessor.createTrainingBackTestingDataFrames(
            stock, earliestDate, dateStart, latestDate, validationDataRatio=0)

        # scale data
        self.trainPeriodStock, self.trainPeriodStockMeanAbsDev = Preprocessor.scaleDataFrameByMeanAbsDev(
            self.relevantColumns, self.trainPeriodStock)
        self.backTestPeriodStock, self.backTestPeriodStockMeanAbsDev = Preprocessor.scaleDataFrameByMeanAbsDev(
            self.relevantColumns, self.backTestPeriodStock)

        # need to implement custom methods to create train and test data
        # create testing / training data
        X_train, X_test, y_train, y_test = train_test_split(
            self.trainPeriodStock[self.indicators], self.trainPeriodStock[self.predictionColumn],
            test_size=0.33, random_state=42)

        self.neuralNetwork = MLPRegressor(hidden_layer_sizes=(60, 45, 30, 15,), max_iter=7000, activation='logistic')
        self.neuralNetwork.fit(X_train, y_train)

        self.evaluateModel(self.neuralNetwork, X_train, y_train, X_test, y_test)

        return

    @overrides(TradingPlan)
    def plan(self, client):
        stockDf = client.stockExchange.getStockByIndex(0).df

        # use neural network to generate prediction
        inputData = stockDf.loc[client.date][self.indicators].values
        inputData = inputData.reshape(1, -1)
        prediction = self.neuralNetwork.predict(inputData)[0]
        # scale back prediction to original representation
        prediction = prediction * self.backTestPeriodStockMeanAbsDev[self.predictionColumn]
        prediction = np.asscalar(prediction)
        actualValue = stockDf.loc[client.date][self.predictionColumn]
        print("prediction:", prediction, "\tactual:", actualValue, "\terror:", abs(actualValue - prediction))

        def _buyCondition():
            return prediction > -5

        def _sellCondition():
            return prediction <= -5

        if _buyCondition():
            if client.averageBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.averageBuyCash())
            elif client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash())

        elif _sellCondition():
            client.setStopLoss(client.portfolio.availableUnitsList[0], 0.995*stockDf.loc[client.date]['close'])

        return
