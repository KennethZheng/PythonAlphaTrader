from tradingplan.TradingPlan import *


class MovingAverage(TradingPlan):
    """
    Trading plan using a moving average crossover strategy to avoid an extended bear market.

    Results:
    This strategy outperforms the market over the long term due to the presence of bear markets. However, this strategy
    under performs slightly during bull markets as a moving average crossover may only be a false positive.
    """

    def __init__(self):
        super().__init__()

    @overrides(TradingPlan)
    def plan(self, client):
        stockDf = client.stockExchange.getStockByIndex(0).df

        def _buyCondition():
            return ((client.portfolio.cash >= client.marketCloses[0])
                    and (client.marketCloses[0] > stockDf.loc[client.date]['sma200'])
                    and (client.date - 2 >= 0)
                    and (stockDf.loc[client.date]['sma14'] > stockDf.loc[client.date - 2]['sma14']))

        def _sellCondition():
            return ((client.portfolio.availableUnitsList[0] >
                     0.6 * (client.maxBuyUnits() + client.portfolio.availableUnitsList[0]))
                    and (stockDf.loc[client.date]['sma7'] < stockDf.loc[client.date]['sma200'])
                    and (client.date - 2 >= 0)
                    and (stockDf.loc[client.date]['sma200'] < stockDf.loc[client.date - 2]['sma200']))

        if _buyCondition():
            if client.averageBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.averageBuyCash())
            elif client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash())

        elif _sellCondition():
            client.setStopLoss(client.portfolio.availableUnitsList[0], 0.98*stockDf.loc[client.date]['sma7'])

        return
