from tensorflow.python.client import device_lib

from tradingplan.Preprocessor import *
from keras import Sequential
from keras.layers import Dropout, Dense, Activation, CuDNNLSTM
import numpy as np
import time

from sklearn.metrics import mean_absolute_error

from tradingplan.TradingPlan import *


# ! Under-performs market
class LstmNetwork(TradingPlan):
    """
    Trading plan using a long short-term memory deep recurrent neural network predict the momentum of the next 7 days.
    The plan buys when it believes the momentum is bullish and sells when it believes the momentum is bearish with
    a bias for buying to account for prediction error.
    """

    def __init__(self):
        super().__init__()
        self.trainPeriodStock = None
        self.trainPeriodStockMeanAbsDev = None
        self.backTestPeriodStock = None
        self.backTestDateStart = None
        self.backTestDateEnd = None
        self.predictDaysAhead = 6
        self.predictionColumn = 'momentum7'
        self.neuralNetwork = None
        self.indicators = [
            'close/open', 'high/low',
            'cci7', 'avgTrueRange7', 'priceROC7', 'variance7',
            'cci14', 'avgTrueRange14', 'momentum14', 'priceROC14', 'variance21',
            'cci21', 'avgTrueRange21', 'momentum21', 'priceROC21', 'variance50',
            'cci50', 'avgTrueRange50', 'momentum50', 'priceROC50', 'variance100',
            'cci100', 'avgTrueRange100', 'momentum100', 'priceROC100',
            'cci200', 'avgTrueRange200', 'momentum200', 'priceROC200',
            'smaRatio7/14', 'smaRatio14/21', 'smaRatio21/50', 'smaRatio50/100', 'smaRatio100/200',
            'percent_volumeE14-21', 'percent_volume14-200', 'percent_volume21-50', 'percent_volume50-200',
            'percent_macd14-21', 'percent_macd14-50', 'percent_macd21-50', 'percent_macd50-100', 'percent_macd50-200',
            'rsi14', 'rsi21', 'rsi50', 'rsi100',
            'smi14', 'smi21', 'smi50', 'smi100', 'smi200',
            'momentumRatio21/50', 'momentumRatio50/100', 'momentumRatio100/200'
        ]
        self.predictionInput = None
        self.relevantColumns = None
        self.relevantColumnsCount = None

    @staticmethod
    def createLstmNeuralNetwork(LSTMVectorLength, relevantColumnsCount):
        neuralNetwork = Sequential()

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            input_shape=(LSTMVectorLength, relevantColumnsCount),
            return_sequences=True))
        neuralNetwork.add(Dropout(0.2))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=True))
        neuralNetwork.add(Dropout(0.2))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=False))
        neuralNetwork.add(Dropout(0.2))

        neuralNetwork.add(Dense(
            units=relevantColumnsCount))
        neuralNetwork.add(Activation('linear'))

        start = time.time()
        neuralNetwork.compile(loss='mse', optimizer='rmsprop')
        print("compilation time:", time.time() - start)
        return neuralNetwork

    @staticmethod
    def createLSTMTrainingInputData(X_train, sequence_length):
        numberOfSequences = X_train.shape[0]
        print("LSTM input # sequences", numberOfSequences)

        LSTM_X_train = np.zeros(shape=(numberOfSequences - sequence_length, sequence_length, X_train.shape[1]))
        for i in range(0, numberOfSequences - sequence_length):
            # print(i)#LSTM_X_train[]
            LSTM_X_train[i, :, :] = X_train[i:i + sequence_length, :]
        print("LSTM input shape", LSTM_X_train.shape)
        return LSTM_X_train

    @staticmethod
    def createLSTMTrainingOutputData(y_train, sequence_length):
        numberOfSequences = y_train.shape[0]
        print("LSTM output # sequences", numberOfSequences)

        LSTM_y_train = np.zeros(shape=(numberOfSequences - sequence_length, y_train.shape[1]))
        for i in range(0, numberOfSequences - sequence_length):
            LSTM_y_train[i, :] = y_train[i + sequence_length, :]
        print("LSTM output shape", LSTM_y_train.shape)
        return LSTM_y_train

    # need to iteratively predictDaysAhead for evaluation
    @staticmethod
    def evaluateModel(neuralNetwork, predictDaysAhead, X_train, y_train, X_test, y_test):
        trainingPredictions = neuralNetwork.predict(X_train)
        print(trainingPredictions[:, -1].shape)
        print(trainingPredictions[:, -1])
        print("Neural network training data mean abs error: ", mean_absolute_error(
            trainingPredictions[:, -1], y_train[:, -1]))

        predictions = neuralNetwork.predict(X_test)
        print("Neural network mean abs error: ", mean_absolute_error(predictions[:, -1], y_test[:, -1]))
        print(y_test[:, -1].shape)
        print("2* standard dev of predictions", 2 * np.std(y_test[:, -1], axis=0))
        return

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        self.backTestDateStart = dateStart
        self.backTestDateEnd = latestDate
        self.relevantColumns = self.indicators + [self.predictionColumn]
        self.relevantColumnsCount = len(self.relevantColumns)

        print(device_lib.list_local_devices())

        stock = client.stockExchange.getStockByIndex(0)

        # partition stock for training and backtest
        self.trainPeriodStock, self.backTestPeriodStock = Preprocessor.createTrainingBackTestingDataFrames(
            stock, earliestDate, dateStart, latestDate, validationDataRatio=0)

        # scale data
        # change backtest period stock to be scaled using trainPeriodStockMeanAbsDev?
        self.trainPeriodStock, self.trainPeriodStockMeanAbsDev = (
            Preprocessor.scaleDataFrameByMeanAbsDev(self.relevantColumns, self.trainPeriodStock))
        self.backTestPeriodStock, backTestPeriodStockMeanAbsDev = (
            Preprocessor.scaleDataFrameByMeanAbsDev(self.relevantColumns, self.backTestPeriodStock))

        # create testing / training data
        LSTMVectorLength = 150
        X_train = self.createLSTMTrainingInputData(self.trainPeriodStock.values, LSTMVectorLength)
        y_train = self.createLSTMTrainingOutputData(self.trainPeriodStock.values, LSTMVectorLength)

        X_test = self.createLSTMTrainingInputData(self.backTestPeriodStock.values, LSTMVectorLength)
        y_test = self.createLSTMTrainingOutputData(self.backTestPeriodStock.values, LSTMVectorLength)
        print(y_test.shape)

        # set prediction input for back testing
        self.predictionInput = X_train[-1:, :, :]

        LSTMVectorLength = X_train.shape[1]

        # create LSTM model
        self.neuralNetwork = self.createLstmNeuralNetwork(LSTMVectorLength, X_train.shape[2])

        self.neuralNetwork.fit(X_train, y_train, batch_size=500, epochs=20)

        # Model Evaluation
        self.evaluateModel(
            self.neuralNetwork, self.predictDaysAhead, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)

        return

    @overrides(TradingPlan)
    def plan(self, client):
        # change lstm to extract prediction from feature and predict x days into the future
        stockDf = client.stockExchange.getStockByIndex(0).df
        prediction = None

        # use neural network to generate prediction
        def _generatePrediction(_predictionInput):
            # assume data has been properly scaled
            _outputPrediction = self.neuralNetwork.predict(_predictionInput)
            _updatedPredictionInput = _predictionInput
            _updatedPredictionInput = np.delete(_updatedPredictionInput, np.s_[0], axis=1)
            _updatedPredictionInput = np.append(_updatedPredictionInput, _outputPrediction[np.newaxis, :, :], axis=1)
            assert (np.array_equal(_updatedPredictionInput[0, -1, :], _outputPrediction[0, :]) is True), (
                "new prediction should be appended to end of predictionInput")
            return _outputPrediction, _updatedPredictionInput

        # first prediction
        outputPrediction, temporaryPredictionInput = _generatePrediction(self.predictionInput)

        # loop self.predictDaysAhead - 1 times
        for i in range(self.predictDaysAhead):
            outputPrediction, temporaryPredictionInput = _generatePrediction(temporaryPredictionInput)

        # update self.predictionInput
        if client.date < self.backTestDateEnd:
            nextInput = self.backTestPeriodStock.loc[client.date + 1][self.relevantColumns].values
            scaledNextInput = Preprocessor.scaleSeriesDataFromMeanAbsDev(nextInput, self.trainPeriodStockMeanAbsDev)
            self.predictionInput = np.delete(self.predictionInput, np.s_[0], axis=1)
            self.predictionInput = np.append(self.predictionInput, scaledNextInput[np.newaxis, np.newaxis, :], axis=1)
            assert (np.array_equal(self.predictionInput[0, -1, :], scaledNextInput[:]) is True), (
                "new prediction should be appended to end of predictionInput")

        # prediction equals the last element of outputPrediction
        prediction = outputPrediction[0, -1]
        # scale back prediction to original representation
        prediction = prediction * self.trainPeriodStockMeanAbsDev.loc[self.predictionColumn]
        prediction = np.asscalar(prediction)

        # print prediction result
        if client.date + self.predictDaysAhead <= self.backTestDateEnd:
            actualValue = stockDf.loc[client.date + self.predictDaysAhead][self.predictionColumn]
            print("prediction: %.3f" % prediction,
                  "\tactual: %.3f" % actualValue,
                  "\terror: %.3f" % (prediction - actualValue))

        def _buyCondition(_predictionColumn):
            return prediction > -1.0

        def _sellCondition(_predictionColumn):
            return prediction <= -2.0

        if _buyCondition(self.predictionColumn):
            if client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash())

        elif _sellCondition(self.predictionColumn):
            client.setStopLoss(client.portfolio.availableUnitsList[0], 0.996*stockDf.loc[client.date]['close'])

        return
