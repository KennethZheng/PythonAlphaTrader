from tradingplan.Preprocessor import *
from tradingplan.TradingPlan import *
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import logging


# ! Under-performs market
# ! This trading plan is exploratory and may require refactoring
class RandomForest(TradingPlan):
    """
    Trading plan using a random forest to predict the future direction of the market. A randomized grid search and
    iterative pruning is used to select the best features. The data is split into training, validation and testing sets.

    Results:
    Although the random forest appears to be effective in classifying the direction of the market 50 days ahead with
    an accuracy of over 90% over a testing data set, the model fails to generalize to the testing time period.
    """

    def __init__(self):
        super().__init__()
        self.randomForest = None
        self.indicators = None
        self.trainingTarget = None
        self.predictDaysAhead = 50
        self.predictionColumn = 'future_category_percent_cdailyChange'+str(self.predictDaysAhead)
        self.decidedAgo = 0
        self.boughtPrice = 0
        self.stopLossPrice = 0
        self.trainPeriodStock = None
        self.backTestPeriodStock = None
        self.backTestDateStart = None
        self.backTestDateEnd = None

    def createRandomForest(self, features):
        X_train, X_test, y_train, y_test = train_test_split(
            features, self.trainingTarget, test_size=0.70)

        # create grid of possible parameters
        n_estimators = [600]
        max_features = ['auto', 'log2', None]
        max_depth = [int(x) for x in np.linspace(3, 7, num=3)]
        min_samples_split = [2, 5, 8, 11]
        min_samples_leaf = [1, 2, 3, 4]
        bootstrap = [True, False]
        random_grid = {'n_estimators': n_estimators,
                       'max_features': max_features,
                       'max_depth': max_depth,
                       'min_samples_split': min_samples_split,
                       'min_samples_leaf': min_samples_leaf,
                       'bootstrap': bootstrap}

        randomForest = RandomForestClassifier()
        randomizedSearch = RandomizedSearchCV(estimator=randomForest, param_distributions=random_grid, n_iter=3, cv=3,
                                              verbose=2, n_jobs=-1)
        randomizedSearch.fit(X_train, y_train)
        y_pred = randomizedSearch.predict(X_test)
        return randomizedSearch, metrics.accuracy_score(y_test, y_pred)

    def pruneFeatures(self, features):
        print("Pruning features:")
        importantFeatures = pd.Series(
            self.randomForest.best_estimator_.feature_importances_, index=features.columns).sort_values(ascending=True)
        prunedFeatures = features
        for val in range(round(len(features.columns)/8)):
            dropIndex = importantFeatures.index[val]
            print("\t"+dropIndex)
            prunedFeatures = prunedFeatures.drop(dropIndex, axis=1, inplace=False)
        return prunedFeatures

    def printImportantFeatures(self):
        importantFeatures = pd.Series(
            self.randomForest.best_estimator_.feature_importances_, index=self.indicators).sort_values(ascending=False)
        sns.barplot(x=importantFeatures, y=importantFeatures.index)
        # Labels
        plt.xlabel('Feature Importance Score')
        plt.ylabel('Features')
        plt.title("Visualizing Important Features for Random Forest")
        plt.show()

    def printBackTestAccuracy(self):
        backtestData = self.backTestPeriodStock[self.indicators].copy(deep=True)
        backtestData['prediction'] = self.randomForest.predict(backtestData)
        backtestData['actual'] = self.backTestPeriodStock[self.predictionColumn]
        backtestData['error'] = 1
        backtestData.loc[backtestData['prediction'] == backtestData['actual'], 'error'] = 0
        backtestData['criticalError'] = 0
        backtestData.loc[
            (backtestData['prediction'] == - backtestData['actual']) & (backtestData['prediction'] != 0),
            'criticalError'] = 1

        backtestData['onlyZeros'] = 0
        backtestData.loc[backtestData['actual'] == 0, 'onlyZeros'] = 1
        backtestData['onlyOnes'] = 0
        backtestData.loc[backtestData['actual'] == 1, 'onlyOnes'] = 1
        backtestData['onlyNegatives'] = 0
        backtestData.loc[backtestData['actual'] == -1, 'onlyNegatives'] = 1

        print("only zeros %", float(backtestData['onlyZeros'].sum() / backtestData.shape[0]) * 100)
        print("only ones %", float(backtestData['onlyOnes'].sum() / backtestData.shape[0]) * 100)
        print("only negatives %", float(backtestData['onlyNegatives'].sum() / backtestData.shape[0]) * 100)
        print("critical error %", float(backtestData['criticalError'].sum() / backtestData.shape[0])*100)
        backtestAccuracy = float(1 - (backtestData['error'].sum() / backtestData.shape[0])) * 100
        print("backtestAccuracy:", backtestAccuracy)

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        stock = client.stockExchange.getStockByIndex(0)

        # columns for visualization purposes
        stock.df['signal'] = stock.df['avg']
        stock.df['real_signal'] = stock.df['avg'] + 100*stock.df[self.predictionColumn]
        logging.debug("stock features: "+str(stock.features))

        self.backTestDateStart = dateStart
        self.backTestDateEnd = latestDate

        stock = client.stockExchange.getStockByIndex(0)

        # partition stock for training and backtest
        self.trainPeriodStock, self.backTestPeriodStock = Preprocessor.createTrainingBackTestingDataFrames(
            stock, earliestDate, dateStart, latestDate, validationDataRatio=0)

        # training data target column
        self.trainingTarget = self.trainPeriodStock[self.predictionColumn]

        # training data input columns
        self.indicators = stock.features
        features = self.trainPeriodStock[self.indicators]
        self.indicators = features.dtypes.index

        # iteratively prune features of random forest
        bestAccuracy = 0
        while len(features.columns) >= 35:
            randomForest, accuracy = self.createRandomForest(features)
            if accuracy > bestAccuracy:
                bestAccuracy = accuracy
                print("bestAccuracy:", bestAccuracy, "%\t # of features:", len(features.columns))
                self.randomForest = randomForest
                self.indicators = features.dtypes.index
                features = self.pruneFeatures(features)
            else:
                break
        print("===== Finished creating final Random Forest =====")

        self.printBackTestAccuracy()
        self.printImportantFeatures()
        return

    @overrides(TradingPlan)
    def plan(self, client):
        if self.decidedAgo > 0:
            self.decidedAgo -= 1

        stockDf = client.stockExchange.getStockByIndex(0).df
        stockDfIndicators = stockDf[self.indicators]
        predictionInput = stockDfIndicators[stockDfIndicators.index == client.date]
        prediction = self.randomForest.predict(predictionInput)
        predictionValue = np.asscalar(prediction[0])

        def _buyCondition():
            if predictionValue == 1:
                stockDf.loc[client.date, 'signal'] = stockDf['signal'][client.date] + 100
            return predictionValue == 1

        def _sellCondition():
            if predictionValue == -1:
                stockDf.loc[client.date, 'signal'] = stockDf['signal'][client.date] - 100
            return predictionValue == -1

        if _sellCondition():    # and client.marketCloses[0] > self.boughtPrice:
            if client.portfolio.availableUnitsList[0] > 0:
                if 0.985*stockDf.loc[client.date]['close'] > self.stopLossPrice:
                    self.stopLossPrice = 0.985 * stockDf.loc[client.date]['close']
                client.setStopLoss(client.portfolio.availableUnitsList[0], self.stopLossPrice)
                self.decidedAgo = 50

        elif _buyCondition() and self.decidedAgo == 0:
            if client.averageBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.averageBuyCash())
                # if client.marketCloses[0] > self.boughtPrice:
                #     self.boughtPrice = client.marketCloses[0]
            elif client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash())
        return
