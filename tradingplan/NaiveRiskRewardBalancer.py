from tradingplan.TradingPlan import *


class NaiveRiskRewardBalancer(TradingPlan):
    """
    Trading plan uses a trailing stop loss to cut losses early and to let profits run.

    Results:
    This simple strategy outperforms the market in a rolling backtest of the SP500 with a cumulative annual growth rate
    of over 12% versus 8%. This difference is significant over the long run due to the nature of geometric growth.
    """

    def __init__(self):
        super().__init__()
        self.valueHolder = ValueHolder()

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        pass

    @overrides(TradingPlan)
    def plan(self, client):
        stockDf = client.stockExchange.getStockByIndex(0).df

        def getStopLoss():
            if self.valueHolder.value > 0:
                if stockDf.loc[client.date]['close'] > self.valueHolder.value:
                    self.valueHolder.value = stockDf.loc[client.date]['close']
                return max(0.99*self.valueHolder.value, 0.99*stockDf.loc[client.date]['close'])
            else:
                return 0.99*stockDf.loc[client.date]['close']

        def _buyCondition():
            if client.portfolio.unitsList[0] == 0:
                return True
            return False

        def _sellCondition():
            if client.portfolio.availableUnitsList[0] > 0:
                return True
            return False

        if _buyCondition():
            if client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash(), valueHolder=self.valueHolder)

        if _sellCondition():
            client.setStopLoss(client.portfolio.availableUnitsList[0], getStopLoss())

        return
