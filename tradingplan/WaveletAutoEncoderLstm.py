import pywt
from keras.optimizers import Adam
from tensorflow.python.client import device_lib

from tradingplan.Preprocessor import *

from keras import Sequential, Model
from keras.layers import Dense, Activation, CuDNNLSTM, Lambda
import numpy as np
import time

from sklearn.metrics import mean_absolute_error, mean_squared_error

from tradingplan.TradingPlan import *
import matplotlib.pyplot as plt


class WaveletAutoEncoderLstm(TradingPlan):
    """
    Trading plan using a long short-term memory deep recurrent neural network to predict the momentum of the next 21
    days. The plan buys when it believes the momentum is bullish and sells when it believes the momentum is bearish with
    a bias for buying to account for prediction error.

    The input data is composed of a stock's technical indicators over different time windows as well as economic
    data and its corresponding features. The model pre-processes the input data by using a multi-level discrete
    wavelet transform to de-noise the high frequency components. A stacked auto-encoder is then used to reduce the
    dimensionality of the features and to extract deep abstract features. These learned features are then passed into a
    deep LSTM neural network.

    This architecture was based on the research paper:
    https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180944
    """

    def __init__(self):
        super().__init__()
        self.trainPeriodStock = None
        self.trainPeriodStockMeanAbsDev = None
        self.backTestPeriodStock = None
        self.backTestDateStart = None
        self.backTestDateEnd = None
        self.predictDaysAhead = 21
        self.predictionColumn = 'sigmoid_momentum21'
        self.neuralNetwork = None
        self.indicators = [
            'cci7', 'avgTrueRange7', 'priceROC7', 'variance7',
            'cci14', 'avgTrueRange14', 'momentum14', 'priceROC14', 'variance21',
            'cci21', 'avgTrueRange21', 'momentum21', 'priceROC21', 'variance50',
            'cci50', 'avgTrueRange50', 'momentum50', 'priceROC50', 'variance100',
            'cci100', 'momentum100', 'priceROC100',
            'cci200', 'momentum200', 'priceROC200',
            'smaRatio7/14', 'smaRatio14/21', 'smaRatio21/50', 'smaRatio50/100', 'smaRatio100/200',
            'percent_volumeE14-21', 'percent_volume21-50', 'percent_volume50-200',
            'percent_macd14-21', 'percent_macd14-50', 'percent_macd21-50', 'percent_macd50-100', 'percent_macd50-200',
            'rsi14', 'rsi21', 'rsi50', 'rsi100',
            'smi14', 'smi21', 'smi50', 'smi100', 'smi200',
            'momentumRatio21/50', 'momentumRatio50/100', 'momentumRatio100/200',
            'trend14Sma14', 'trend21Sma21', 'trend50Sma50', 'trend100Sma100',
            'percent_cdailyChange7', 'percent_cdailyChange14', 'percent_cdailyChange21', 'percent_cdailyChange50',
            'percent_cdailyChange100', 'percent_cdailyChange200',
            'percent_cmfv7', 'percent_cmfv14', 'percent_cmfv21', 'percent_cmfv50', 'percent_cmfv100', 'percent_cmfv200'
        ]
        self.externalIndicators = [
            'AAA_daaa',
            'AAA_cci14', 'AAA_priceROC14', 'AAA_variance14', 'AAA_momentum14',
            'AAA_cci21', 'AAA_priceROC21', 'AAA_variance21', 'AAA_momentum21',
            'AAA_cci50', 'AAA_priceROC50', 'AAA_variance50', 'AAA_momentum50',

            'LIBOR_usd1mtd156n',
            'LIBOR_cci14', 'LIBOR_priceROC14', 'LIBOR_variance14', 'LIBOR_momentum14',
            'LIBOR_cci21', 'LIBOR_priceROC21', 'LIBOR_variance21', 'LIBOR_momentum21',
            'LIBOR_cci50', 'LIBOR_priceROC50', 'LIBOR_variance50', 'LIBOR_momentum50',

            'TB3_dtb3',
            'TB3_cci14', 'TB3_priceROC14', 'TB3_variance14', 'TB3_momentum14',
            'TB3_cci21', 'TB3_priceROC21', 'TB3_variance21', 'TB3_momentum21',
            'TB3_cci50', 'TB3_priceROC50', 'TB3_variance50', 'TB3_momentum50',
        ]

        self.predictionInput = None
        self.relevantColumns = None
        self.relevantColumnsCount = None
        self.waveletCoeffs = None
        self.emphasisMultiplier = 5
        self.preWaveletLSTMVectorLength = None
        self.LSTMVectorLength = None
        self.inputFeatureCount = None
        self.autoEncoder = None

    def createPredictionInput(self, stockDf, date):
        stockDf = stockDf[self.relevantColumns]
        stockDf = stockDf[stockDf.index >= date]
        stockValues = stockDf.values
        LSTM_X_train = np.zeros(shape=(1, self.preWaveletLSTMVectorLength, stockValues.shape[1]))
        LSTM_X_train[0, :, :] = stockValues[0:self.preWaveletLSTMVectorLength, :]
        # transform data
        LSTM_X_train = self.multiLevelDiscreteWaveletTransform(LSTM_X_train, output=False)
        LSTM_X_train = self.autoEncoder.predict(LSTM_X_train)
        return LSTM_X_train

    def createLSTMTrainingInputData(self, X_train, sequence_length):
        numberOfSequences = X_train.shape[0]
        print("LSTM input # sequences", numberOfSequences)

        LSTM_X_train = np.zeros(shape=(numberOfSequences - sequence_length, sequence_length, X_train.shape[1]))
        for i in range(0, numberOfSequences - sequence_length - self.predictDaysAhead + 1):
            LSTM_X_train[i, :, :] = X_train[i:i + sequence_length, :]
        print("LSTM input shape", LSTM_X_train.shape)
        return LSTM_X_train

    def createLSTMTrainingOutputData(self, y_train, sequence_length):
        numberOfSequences = y_train.shape[0]
        print("LSTM output # sequences", numberOfSequences)

        LSTM_y_train = np.zeros(shape=(numberOfSequences - sequence_length, sequence_length))
        print("LSTM output shape", LSTM_y_train.shape)
        for i in range(0, numberOfSequences - sequence_length - self.predictDaysAhead + 1):
            LSTM_y_train[i, :] = y_train[i+self.predictDaysAhead:i + sequence_length+self.predictDaysAhead, -1]
        print("LSTM output shape", LSTM_y_train.shape)
        return LSTM_y_train

    @staticmethod
    def multiLevelDiscreteWaveletTransform(ndarray, output=True):
        if output is True:
            print("prewavelet shape", ndarray.shape)
        coeffs = pywt.wavedec(ndarray, 'haar', level=2, axis=1)
        # de-noise wavelets
        coeffs[1:] = (pywt.threshold(i, value=0.005, mode="hard") for i in coeffs[1:])
        cA2, cD1, cD2 = coeffs
        reconstructed = np.concatenate((cA2, cD1, cD2), axis=1)
        if output is True:
            print("wavelet shape", reconstructed.shape)
        return reconstructed

    @staticmethod
    def createAutoEncoder(X_input):
        print("autoencoder input", X_input.shape)
        neuralNetwork = Sequential()
        neuralNetwork.add(Dense(X_input.shape[2], activation='tanh', input_shape=(X_input.shape[1], X_input.shape[2])))
        # neuralNetwork.add(Dense(round(X_input.shape[2]*0.8), activation='elu'))
        neuralNetwork.add(Dense(round(X_input.shape[2]*0.6), activation='tanh', name="bottleneck"))
        # neuralNetwork.add(Dense(round(X_input.shape[2]*0.8), activation='elu'))
        neuralNetwork.add(Dense(X_input.shape[2], activation='tanh'))
        neuralNetwork.compile(loss='mse', optimizer=Adam())
        neuralNetwork.fit(X_input, X_input, batch_size=500, epochs=15, verbose=1, validation_data=(X_input, X_input))

        autoEncoder = Model(neuralNetwork.input, neuralNetwork.get_layer('bottleneck').output)
        return autoEncoder

    def createStackedAutoEncoder(self, X_input, layers=2):
        print("stacked autoencoder input", X_input.shape)
        stackedAutoEncoder = StackedAutoEncoder()
        for i in range(layers):
            print("layer", i+1)
            autoEncoder = self.createAutoEncoder(X_input)
            X_input = autoEncoder.predict(X_input)
        return stackedAutoEncoder

    @staticmethod
    def createLstmNeuralNetwork(LSTMVectorLength, featureCount):
        neuralNetwork = Sequential()

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            input_shape=(LSTMVectorLength, featureCount),
            return_sequences=True))
        # neuralNetwork.add(Dropout(0.02))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=True))
        # neuralNetwork.add(Dropout(0.02))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=True))
        # neuralNetwork.add(Dropout(0.02))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=True))
        # neuralNetwork.add(Dropout(0.02))

        neuralNetwork.add(CuDNNLSTM(
            LSTMVectorLength,
            return_sequences=True))
        # neuralNetwork.add(Dropout(0.02))

        # neuralNetwork.add(Dense(
        #     units=featureCount))
        neuralNetwork.add(Lambda(lambda x: x[:, :, -1]))
        neuralNetwork.add(Activation('tanh'))

        start = time.time()
        neuralNetwork.compile(loss='mse', optimizer='rmsprop')
        print("compilation time:", time.time() - start)

        return neuralNetwork

    @staticmethod
    def evaluateModel(neuralNetwork, predictDaysAhead, X_train, y_train, X_test, y_test):
        trainingPredictions = neuralNetwork.predict(X_train)

        # only take the final prediction (predictDaysAhead in the future)
        trainingPredictions = trainingPredictions[:, -1]
        relevant_y_train = y_train[:, -1]
        print("relevant y_train shape", relevant_y_train.shape)

        print(trainingPredictions.shape)
        print("Neural network training data mean abs error: ",
              mean_absolute_error(trainingPredictions, relevant_y_train))
        print("Neural network training data rmse: ", mean_squared_error(trainingPredictions, relevant_y_train)**0.5)

        trainingErrors = np.abs(trainingPredictions - relevant_y_train)
        plt.figure(figsize=(50, 15))
        plt.hist(trainingErrors, bins='auto')
        plt.title("Training prediction error distribution")
        plt.show()

        testPredictions = neuralNetwork.predict(X_test)

        testPredictions = testPredictions[:, -1]
        relevant_y_test = y_test[:, -1]

        plt.figure(figsize=(50, 15))
        plt.hist(testPredictions, bins='auto')
        plt.title("test predictions distribution")
        plt.show()

        print("\nNeural network test data mean abs error: ",
              mean_absolute_error(testPredictions, relevant_y_test))
        print("Neural network test data rmse: ", mean_squared_error(testPredictions, relevant_y_test)**0.5)

        testErrors = np.abs(testPredictions - relevant_y_test)
        plt.figure(figsize=(50, 15))
        plt.hist(testErrors, bins='auto')
        plt.title("Test prediction error distribution")
        plt.show()

        print(relevant_y_test.shape)
        print("2* standard dev of test column", 2 * np.std(relevant_y_test, axis=0))

        plt.figure(figsize=(50, 15))
        plt.hist(relevant_y_test, bins='auto')
        plt.title("Relevant test data distribution")
        plt.show()

        def trivialPrediction(_predictDaysAhead, _relevant_y_test):
            # set prediction as the correct value from "predictDaysAhead" days in the past
            _trivialPrediction = np.copy(_relevant_y_test)
            for i, predictions in enumerate(_relevant_y_test):
                if i < _predictDaysAhead:
                    _trivialPrediction[i] = 0
                else:
                    _trivialPrediction[i] = _relevant_y_test[i - _predictDaysAhead]
            return _trivialPrediction

        trivialPred = trivialPrediction(predictDaysAhead, relevant_y_test)
        print(trivialPred.shape)
        print("trivial model test data mean abs error: ",
              mean_absolute_error(trivialPred, relevant_y_test))
        print("trivial model network test data rmse: ", mean_squared_error(trivialPred, relevant_y_test)**0.5)

        trivialPredErrors = np.abs(trivialPred - relevant_y_test)
        plt.figure(figsize=(50, 15))
        plt.hist(trivialPredErrors, bins='auto')
        plt.title("Trivial prediction error distribution")
        plt.show()

        zerosPred = np.zeros(relevant_y_test.shape)
        print(zerosPred.shape)
        print("zeros model test data mean abs error: ",
              mean_absolute_error(zerosPred, relevant_y_test))
        print("zeros model network test data rmse: ", mean_squared_error(zerosPred, relevant_y_test)**0.5)

        zerosPredErrors = np.abs(zerosPred - relevant_y_test)
        plt.figure(figsize=(50, 15))
        plt.hist(zerosPredErrors, bins='auto')
        plt.title("Zeros prediction error distribution")
        plt.show()

        meanPred = np.full(relevant_y_test.shape, fill_value=relevant_y_train.mean())
        print(meanPred.shape)
        print("train mean", relevant_y_train.mean())
        print("mean model test data mean abs error: ",
              mean_absolute_error(meanPred, relevant_y_test))
        print("mean model network test data rmse: ", mean_squared_error(meanPred, relevant_y_test)**0.5)

        meanPredErrors = np.abs(meanPred - relevant_y_test)
        plt.figure(figsize=(50, 15))
        plt.hist(meanPredErrors, bins='auto')
        plt.title("Mean prediction error distribution")
        plt.show()

        return

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        self.backTestDateStart = dateStart
        self.backTestDateEnd = latestDate
        self.relevantColumns = self.indicators + self.externalIndicators + [self.predictionColumn]
        self.relevantColumnsCount = len(self.relevantColumns)

        print(device_lib.list_local_devices())

        stock = client.stockExchange.getStockByIndex(0)
        self.indicators = self.indicators + self.externalIndicators

        # partition stock for training and backtest
        self.trainPeriodStock, self.backTestPeriodStock = Preprocessor.createTrainingBackTestingDataFrames(
            stock, earliestDate, dateStart, latestDate, validationDataRatio=0)

        # scale data using tanh estimators - fix backtestperiodstock scaling to use values from trainperiodstock
        self.trainPeriodStock = Preprocessor.scaleDataFrameByTanhEstimators(
            self.indicators, self.trainPeriodStock, unscaledColumns=[self.predictionColumn])
        self.backTestPeriodStock = Preprocessor.scaleDataFrameByTanhEstimators(
            self.indicators, self.backTestPeriodStock, unscaledColumns=[self.predictionColumn])

        # DataFrameFeatureVisualizer(self.trainPeriodStock, figSizeTuple=(50, 15))\
        #     .printFeatureListStats(self.relevantColumns, showHist=False)

        # create LSTM testing / training data
        self.preWaveletLSTMVectorLength = 180
        X_train = self.createLSTMTrainingInputData(self.trainPeriodStock.values, self.preWaveletLSTMVectorLength)
        y_train = self.createLSTMTrainingOutputData(self.trainPeriodStock.values, self.preWaveletLSTMVectorLength)

        X_test = self.createLSTMTrainingInputData(self.backTestPeriodStock.values, self.preWaveletLSTMVectorLength)
        y_test = self.createLSTMTrainingOutputData(self.backTestPeriodStock.values, self.preWaveletLSTMVectorLength)
        print(y_test.shape)

        # multilevel discrete wavelet transform on input data
        X_train = self.multiLevelDiscreteWaveletTransform(X_train)
        X_test = self.multiLevelDiscreteWaveletTransform(X_test)

        self.autoEncoder = self.createStackedAutoEncoder(X_train, layers=4)
        X_train = self.autoEncoder.predict(X_train)
        X_test = self.autoEncoder.predict(X_test)

        self.LSTMVectorLength = X_train.shape[1]
        self.inputFeatureCount = X_train.shape[2]

        # create LSTM model
        self.neuralNetwork = self.createLstmNeuralNetwork(self.LSTMVectorLength, self.inputFeatureCount)

        self.neuralNetwork.fit(X_train, y_train, batch_size=500, epochs=10)

        # save model
        # self.neuralNetwork.save('WaveletAutoEncoderLstm.h5')
        # self.neuralNetwork = load_model('WaveletAutoEncoderLstm.h5')

        # Model Evaluation
        self.evaluateModel(
            self.neuralNetwork, self.predictDaysAhead, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)

        return

    @overrides(TradingPlan)
    def plan(self, client):
        stockDf = client.stockExchange.getStockByIndex(0).df
        predictionInput = self.createPredictionInput(stockDf, client.date)

        prediction = self.neuralNetwork.predict(predictionInput)
        prediction = prediction[0, -1]
        prediction = np.asscalar(prediction)

        # print prediction result
        if client.date + self.predictDaysAhead <= self.backTestDateEnd:
            actualValue = stockDf.loc[client.date + self.predictDaysAhead][self.predictionColumn]
            print("prediction: %.3f" % prediction,
                  "\tactual: %.3f" % actualValue,
                  "\terror: %.3f" % (prediction - actualValue))

        def _buyCondition(_predictionColumn):
            return prediction >= 0

        def _sellCondition(_predictionColumn):
            return prediction < 0

        if _buyCondition(self.predictionColumn):
            if client.maxBuyCash() > client.marketHighs[0]:
                client.midPointBuy(client.maxBuyCash())

        elif _sellCondition(self.predictionColumn):
            client.setStopLoss(client.portfolio.availableUnitsList[0], 0.999*stockDf.loc[client.date]['close'])

        return


class StackedAutoEncoder:
    def __init__(self):
        self.autoEncoders = list()

    def add(self, autoEncoder):
        self.autoEncoders.append(autoEncoder)

    def predict(self, X_train):
        for autoEncoder in self.autoEncoders:
            X_train = autoEncoder.predict(X_train)
        return X_train
