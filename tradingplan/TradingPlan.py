from abc import abstractmethod
from util import *


class TradingPlan:
    """
    Base class used to initialize variables,
    ensure an initPlan method exists,
    and ensure the plan method is overridden
    """

    def __init__(self):
        if type(self) is TradingPlan:
            raise TypeError("Base class may not be instantiated")
        self.perm = None
    
    def initPlan(self, client, earliestDate, dateStart, latestDate):
        """
        Public method for client to initialize the dependencies of a trading plan.
        This empty implementation ensures a valid method exists. Can be overridden.

        :param client:
        :param earliestDate:    earliest date for training data
        :param dateStart:       start date for backtest
        :param latestDate:      end date for backtest
        """
        return
    
    @abstractmethod
    def plan(self, client):
        """
        Public method for client to plan. This method must be overridden by the subclass.

        :param client:
        :return:
        """
        pass


class SavingsAccount(TradingPlan):
    """
    This plan places the investor's capital in a savings account as a minimum baseline.
    """

    def __init__(self):
        super().__init__()

    @overrides(TradingPlan)
    def plan(self, client):
        return


class BuyAndHold(TradingPlan):
    """
    A buy and hold strategy which acts as the benchmark for other trading plans to beat.
    """

    def __init__(self):
        super().__init__()
        self.backTestDateStart = None
        self.backTestDateEnd = None
        self.predictDaysAhead = 50

    def initPlan(self, client, earliestDate, dateStart, latestDate):
        self.backTestDateStart = dateStart
        self.backTestDateEnd = latestDate
        return

    @overrides(TradingPlan)
    def plan(self, client):
        if client.maxBuyCash() > client.marketPrices[0]:
            client.midPointBuy(client.maxBuyCash())
        return
