import numpy as np


class DataFrameExtractor:
    """
    Extracts date and index information from dataframe. Can also return a custom index window of the dataframe.
    Inherited by Stock.
    """

    def __init__(self, _df):
        self.df = _df   # .copy(deep=True)
        self._kind = None
        self.value = None
        
    def getLength(self):
        self.value = len(self.df)
        self._kind = type(self.value)
        # print("getLength", self._kind)
        return self
    
    def getFirstIndex(self):
        self.value = self.df.index[0]
        self._kind = type(self.value)
        # print("getFirstIndex", self._kind)
        return self

    def getLastIndex(self):
        self.value = self.df.index[len(self.df)-1]
        self._kind = type(self.value)
        # print("getLastIndex", self._kind)
        return self

    def getDateByIndex(self):
        assert(self._kind == np.int64), "Type Error: expected value of type <class 'np.int64'>, got %s" % self._kind
        index = self.value
        self.value = self.df.loc[index, 'date'].to_datetime64()
        self._kind = type(self.value)
        # print("getDateByIndex", self._kind)
        return self

    def getIndexByDate(self):
        assert(self._kind == np.datetime64), (
                "Type Error: expected value of type <class 'np.datetime64'>, got %s" % self._kind)
        timeStamp = self.value
        self.value = self.df.index[(self.df['date'] == timeStamp)][0]
        self._kind = type(self.value)
        # ("getIndexByDate", self._kind)
        # print("index", timeStamp)
        return self

    def getTimePeriod(self, _startIndex, _endIndex):
        assert((_startIndex >= self.getFirstIndex().value) & (_endIndex <= self.getLastIndex().value)), (
            "Index Error: invalid time period")
        return self.df[(self.df.index >= _startIndex) & (self.df.index <= _endIndex)]
