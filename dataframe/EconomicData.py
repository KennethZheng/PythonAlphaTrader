from dataframe.DataFrameFactory import *
import util


class EconomicData:
    """
    Builds the feature columns of a dataframe containing economic data.
    """

    def __init__(self, name, acronym, csvFilePath, datesDataFrame, dataColumn):
        self.acronym = acronym
        self.prefix = self.acronym + '_'
        self.name = name
        self.dataColumn = dataColumn
        self.df = pd.read_csv(csvFilePath)
        DataFrameFactory.setDateTime(self.df, dateColumn='DATE')
        DataFrameFactory.columnNamesToLowerCase(self.df)
        self.dateType = type(self.df.iloc[0]['date'])

        self._dropInitialNullRows()
        self.interpolateNullValues()

        self.timePeriods = [7, 14, 21, 50, 100, 200]
        self.timePeriodsCount = len(self.timePeriods)
        self.features = list()
        self._setFeatureColumns()
        self.prefixColumns()
        print(self.features)
        print(self.df.columns)
        self.alignWithDateSeries(datesDataFrame)
        self.verifyData()

    def alignWithDateSeries(self, datesDataFrame):
        self.df = pd.merge(datesDataFrame, self.df, on='date', how='left')
        self.df[self.df.applymap(
            lambda x: False if type(x) is self.dateType else not util.isNumber(x))] = np.nan
        print("economic data shape", self.df.shape)

    def interpolateNullValues(self):
        self.df[self.df.applymap(
            lambda x: False if type(x) is self.dateType else not util.isNumber(x))] = np.nan
        # at this point there exists a numeric value directly or indirectly before any nan
        # to do: report number of nans we interpolate

        def setAsPreviousNonNanValue(dataFrame, columnName, index):
            if not pd.isnull(dataFrame.loc[index-1][columnName]):
                # print("replace na with value at index", i-1, "with value:", dataFrame.loc[index-1][columnName],
                #       "of type:", type(dataFrame.loc[index-1][columnName]))
                return dataFrame.loc[index-1][columnName]
            else:
                return setAsPreviousNonNanValue(dataFrame, columnName, index-1)

        # to do: improve efficiency
        for column in self.df.columns:
            if type(self.df.iloc[0][column]) is not self.dateType:
                # print("type of column", type(self.df.iloc[0][column]))
                for i in self.df.index:
                    if pd.isnull(self.df.loc[i][column]):
                        # print("NAN at", i)
                        self.df.loc[i, column] = setAsPreviousNonNanValue(self.df, column, i)
                        # print("new value", self.df.loc[i][column]), "of type:", type(self.df.loc[i][column])

        print("econ df available size", self.df.shape)
        DataFrameFactory.assertNotContainingNull(self.df)

    def verifyData(self):
        DataFrameFactory.assertNotContainingNull(self.df)

    def _dropInitialNullRows(self):
        earliestNonNullDateIndex = self.df.dropna().index.min()
        self.df.drop(self.df[self.df.index < earliestNonNullDateIndex].index, inplace=True)
        for column in self.df.columns:
            if type(self.df[column].iloc[0]) is not self.dateType:
                self.df[column] = self.df[column].apply(pd.to_numeric, errors='coerce')

    def getEarliestDate(self):
        return self.df.iloc[0]['date']

    def prefixColumns(self):
        self.df = self.df.rename(lambda x: x if x == 'date' else self.prefix+x, axis='columns')
        self.features = list(map(lambda x: self.prefix+x, self.features))

    def _setFeatureColumns(self):
        self.features = list(self.df.drop(columns=['date']).columns.values)
        for window in self.timePeriods:
            self.setSma(window)
            self.setCommodityChannelIndex(window)
            self.setPriceRateOfChange(window)
            self.setAvgVariance(window)
            self.setMomentum(window)

        self.setMovingAverageRatio(7, 14)
        self.setMovingAverageRatio(14, 21)
        self.setMovingAverageRatio(21, 50)
        self.setMovingAverageRatio(50, 100)
        self.setMovingAverageRatio(100, 200)

        self.setMacd(14, 21)
        self.setMacd(14, 50)
        self.setMacd(21, 50)
        self.setMacd(50, 100)
        self.setMacd(50, 200)

        self.setMomentumRatio(21, 50)
        self.setMomentumRatio(50, 100)
        self.setMomentumRatio(100, 200)

    def _addFeature(self, columnName):
        assert(columnName in self.df), "Error: dataframe must contain column: "+columnName
        self.features.append(columnName)

    def setSma(self, window):
        self.df['sma'+str(window)] = self.df[self.dataColumn].rolling(window=window, min_periods=1).mean()
        return self

    # macd: moving average convergence divergence
    def setMacd(self, shortWindow, longWindow):
        self.df['macd'+str(shortWindow)+'-'+str(longWindow)] = (
            self.df['sma'+str(shortWindow)] - self.df['sma'+str(longWindow)])
        return self

    def setMovingAverageRatio(self, shortWindow, longWindow):
        columnName = 'smaRatio'+str(shortWindow)+'/'+str(longWindow)
        self.df[columnName] = self.df['sma'+str(shortWindow)] / self.df['sma'+str(longWindow)]
        self._addFeature(columnName)
        return self

    def setCommodityChannelIndex(self, window):
        # helps find the start and end of trends
        columnName = 'cci'+str(window)
        Difference = (self.df[self.dataColumn] - self.df['sma'+str(window)])
        AbsoluteDifference = Difference.abs()
        MeanDeviation = AbsoluteDifference.rolling(window=window, min_periods=1).sum() / window
        self.df[columnName] = Difference / (0.015 * MeanDeviation + util.EPSILON)
        self._addFeature(columnName)
        return self

    def setPriceRateOfChange(self, window):
        # may produce null values in the initial dataframe rows
        columnName = 'priceROC' + str(window)
        self.df[columnName] = ((self.df[self.dataColumn] - self.df[self.dataColumn].shift(window))
                               / (self.df[self.dataColumn].shift(window) + util.EPSILON))
        self._addFeature(columnName)
        return self

    def setAvgVariance(self, window):
        columnName = 'variance'+str(window)
        self.df[columnName] = (
            ((100*(self.df[self.dataColumn] - self.df['sma'+str(window)]) / self.df['sma'+str(window)])**2) / window)
        self._addFeature(columnName)
        return self

    def setMomentum(self, window):
        # may produce null values in the initial dataframe rows
        columnName = 'momentum'+str(window)
        self.df[columnName] = (self.df[self.dataColumn] / (
                self.df[self.dataColumn].shift(window) + util.EPSILON) - 1) * 100
        self._addFeature(columnName)
        return self

    def setMomentumRatio(self, shortWindow, longWindow):
        # may produce null values in the initial dataframe rows
        columnName = 'momentumRatio'+str(shortWindow)+'/'+str(longWindow)
        # note the momentum values used differ from those created in setMomentum
        shortWindowMomentum = self.df[self.dataColumn] / (self.df[self.dataColumn].shift(shortWindow) + util.EPSILON)
        longWindowMomentum = self.df[self.dataColumn] / (self.df[self.dataColumn].shift(longWindow) + util.EPSILON)
        self.df[columnName] = shortWindowMomentum / (longWindowMomentum + util.EPSILON)
        self._addFeature(columnName)
        return self
