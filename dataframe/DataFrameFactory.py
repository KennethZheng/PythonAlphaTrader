import numpy as np
import pandas as pd
from abc import ABCMeta
import logging


class DataFrameFactory(metaclass=ABCMeta):
    """
    Static API class to create Pandas dataframe containing stock price and volume data

    Public Methods:
        createDataFrame
        finalizeDataFrame
        reduceTimePeriod
    """
    
    @classmethod
    def createDataFrame(cls, filename):
        """
        Creates a pandas dataframe of price and volume information from a csv file.

        :param filename: path to csv file
        :return: reference to dataframe
        """

        dataframe = pd.read_csv(filename)
        logging.debug("raw data shape:", dataframe.shape)
        cls.assertNotContainingNull(dataframe)
        cls.setDateTime(dataframe, dateColumn='Date')
        cls.columnNamesToLowerCase(dataframe)
        cls.assertNotContainingNull(dataframe)
        return dataframe

    @classmethod
    def finalizeDataFrame(cls, dataframe, remove, decimalPlaces=4):
        """
        Remove a dataframe's initial rows, round all values to 4 decimal places and reset the index.

        :param dataframe: reference to the dataframe
        :param remove: initial rows to be removed
        :param decimalPlaces: optional argument to specify decimal places to round values to
        :return: reference to dataframe
        """
        cls._removeInitialRows(dataframe, remove)
        dataframe = cls._roundDecimalPlaces(dataframe, decimalPlaces)
        cls._resetIndex(dataframe)
        cls.assertNotContainingNull(dataframe)
        return dataframe

    @classmethod
    def reduceTimePeriod(cls, dataframe, size):
        """
        Reduce the time window of the dataframe to the specified size by dropping the initial rows first.

        :param dataframe:
        :param size: number of trading days
        :return: reference to dataframe
        """
        dataframe.drop(dataframe.index[:(len(dataframe)-size)], inplace=True)
        cls._resetIndex(dataframe)
        assert(len(dataframe) == size)
        return dataframe

    @staticmethod
    def _removeInitialRows(dataframe, remove):
        if remove > 0:
            logging.info("removing first", remove, "days")
        dataframe.drop(dataframe.index[:remove], inplace=True)
        logging.info("dataframe new shape:", dataframe.shape)

    @staticmethod
    def _resetIndex(dataframe):
        dataframe.reset_index(inplace=True)
    
    @staticmethod
    def assertNotContainingNull(dataframe):
        assert((dataframe.isnull().values.any()) == np.bool_(False)), (
                "Error: dataframe contains null values"
                + "\nDEBUG: columns with null values" + str(dataframe.columns[dataframe.isnull().any()].tolist())
                + "\nDEBUG: dataframe.isnull().values.any() = " + str(dataframe.isnull().values.any()))
    
    @staticmethod
    def setDateTime(dataframe, dateColumn):
        dataframe[dateColumn] = pd.to_datetime(dataframe[dateColumn])
        
    @staticmethod
    def columnNamesToLowerCase(dataframe):
        dataframe.columns = [('adjClose' if x == 'Adj Close' else x.lower()) for x in dataframe.columns]

    @staticmethod
    def _roundDecimalPlaces(dataframe, decimalPlaces):
        return dataframe.round(decimals=decimalPlaces)
