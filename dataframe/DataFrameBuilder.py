from math import log
from math import e as E

import pandas as pd
# indicators in progress
from util import EPSILON


class DataFrameBuilder:
    """
    Builds the feature columns of a dataframe containing stock data.
    Inherited by Stock.
    """

    def __init__(self, _df):
        self.df = _df
        self.timePeriods = [7, 14, 21, 50, 100, 200]
        # [5, 7, 14, 21, 35, 50, 75, 100, 150, 200]
        self.timePeriodsCount = len(self.timePeriods)
        self.features = list()
        self.futureFeatures = list()
        self._setDefaultColumns()

    def setPercentCDailyChange(self, window):
        columnName = 'percent_cdailyChange'+str(window)
        percentDailyChangeMultiplier = self.df['percent_dailyChange'] + 1
        self.df[columnName] = percentDailyChangeMultiplier.rolling(
            window=window, min_periods=1).apply(lambda x: x.prod(), raw=False)
        self.df[columnName] = self.df[columnName] - 1
        self._addFeature(columnName)
        return self

    def setVolumeSma(self, window):
        self.df['volumeSma'+str(window)] = self.df['volume'].rolling(window=window, min_periods=1).mean()
        return self
        
    def setVolumeEma(self, window):
        self.df['volumeEma'+str(window)] = pd.Series.ewm(self.df.volume, span=window, min_periods=1).mean()
        return self
        
    def setPercentVolumeEmaSmaMacd(self, windowEma, windowSma):
        columnName = 'percent_volumeE'+str(windowEma)+'-'+str(windowSma)
        self.df[columnName] = ((self.df['volumeEma'+str(windowEma)] - self.df['volumeSma'+str(windowSma)])
                               / self.df['volumeSma'+str(windowSma)])
        self._addFeature(columnName)
        return self
        
    def setPercentVolumeSmaMacd(self, shortWindow, longWindow):
        columnName = 'percent_volume'+str(shortWindow)+'-'+str(longWindow)
        self.df[columnName] = ((self.df['volumeSma'+str(shortWindow)] - self.df['volumeSma'+str(longWindow)])
                               / self.df['volumeSma'+str(longWindow)])
        self._addFeature(columnName)
        return self

    def setVolumeWeightedPercentSma(self, window):
        columnName = 'volumeWeightedPercentSma'+str(window)
        maxVolume = self.df['volume'].rolling(window=window, min_periods=1).max()
        weightedSma = (self.df['volume'] / maxVolume) * self.df['sma'+str(window)]
        self.df[columnName] = (weightedSma.rolling(window=window, min_periods=1).sum()) / self.df['sma'+str(window)]
        self._addFeature(columnName)
        return self
        
    # sma: simple moving average
    def setAvgSma(self, window):
        self.df['sma'+str(window)] = self.df['avg'].rolling(window=window, min_periods=1).mean()
        return self

    # typical price simple moving average
    def setTpSma(self, window):
        self.df['tpSma'+str(window)] = self.df['tp'].rolling(window=window, min_periods=1).mean()
        return self
    
    # ema: exponential moving average
    def setAvgEma(self, window):
        columnName = 'ema'+str(window)
        self.df[columnName] = pd.Series.ewm(self.df.avg, span=window, min_periods=1).mean()
        return self
        
    # macd: moving average convergence divergence
    def setMacd(self, shortWindow, longWindow):
        self.df['macd'+str(shortWindow)+'-'+str(longWindow)] = (
            self.df['sma'+str(shortWindow)] - self.df['sma'+str(longWindow)])
        return self

    # fix percent macd to use a suitable denominator as it is currently similar to setMovingAverageRatio
    def setPercentMacd(self, shortWindow, longWindow):
        columnName = 'percent_macd'+str(shortWindow)+'-'+str(longWindow)
        self.df[columnName] = (
            self.df['macd'+str(shortWindow)+'-'+str(longWindow)] / self.df['sma'+str(longWindow)])
        self._addFeature(columnName)
        return self

    def setMovingAverageRatio(self, shortWindow, longWindow):
        columnName = 'smaRatio'+str(shortWindow)+'/'+str(longWindow)
        self.df[columnName] = self.df['sma'+str(shortWindow)] / self.df['sma'+str(longWindow)]
        self._addFeature(columnName)
        return self

    def setMomentum(self, window):
        # may produce null values in the initial dataframe rows
        columnName = 'momentum'+str(window)
        self.df[columnName] = (self.df['close'] / self.df['close'].shift(window) - 1) * 100
        self._addFeature(columnName)
        return self

    def setMomentumRatio(self, shortWindow, longWindow):
        # may produce null values in the initial dataframe rows
        columnName = 'momentumRatio'+str(shortWindow)+'/'+str(longWindow)
        # note the momentum values used differ from those created in setMomentum
        shortWindowMomentum = self.df['close'] / self.df['close'].shift(shortWindow)
        longWindowMomentum = self.df['close'] / self.df['close'].shift(longWindow)
        self.df[columnName] = shortWindowMomentum / longWindowMomentum
        self._addFeature(columnName)
        return self

    def setStochasticMomentumIndex(self, window):
        columnName = 'smi' + str(window)

        # DifferenceDoubleEma = EMA( EMA( Close - ( (highBand + lowBand) / 2 ) ))
        MidPoint = (self.df['highBand'+str(window)] + self.df['lowBand'+str(window)]) / 2
        Difference = self.df['close'] - MidPoint
        DifferenceEma = pd.Series.ewm(Difference, span=window, min_periods=1).mean()
        DifferenceDoubleEma = pd.Series.ewm(DifferenceEma, span=window, min_periods=1).mean()

        # RangeDoubleEma = EMA( EMA(highBand - lowBand))
        Range = self.df['highBand' + str(window)] - self.df['lowBand' + str(window)]
        RangeEma = pd.Series.ewm(Range, span=window, min_periods=1).mean()
        RangeDoubleEma = pd.Series.ewm(RangeEma, span=window, min_periods=1).mean()

        self.df[columnName] = DifferenceDoubleEma / RangeDoubleEma
        self._addFeature(columnName)
        return self

    def setPriceRateOfChange(self, window):
        # may produce null values in the initial dataframe rows
        columnName = 'priceROC' + str(window)
        self.df[columnName] = (self.df['close'] - self.df['close'].shift(window)) / self.df['close'].shift(window)
        self._addFeature(columnName)
        return self
        
    def setAvgVariance(self, window):
        columnName = 'variance'+str(window)
        self.df[columnName] = (
            ((100*(self.df['avg'] - self.df['sma'+str(window)]) / self.df['sma'+str(window)])**2) / window)
        self._addFeature(columnName)
        return self

    def setStandardDeviation(self, window):
        columnName = 'std' + str(window)
        Deviations = self.df['close'] - self.df['sma'+str(window)]
        SquaredDeviations = Deviations ** 2
        SumOfSquaredDeviations = SquaredDeviations.rolling(window=window, min_periods=window).sum()
        MeanOfSquaredDeviations = SumOfSquaredDeviations / window
        self.df[columnName] = MeanOfSquaredDeviations ** 0.5
        return self

    # Bollinger middle band is same as the simple moving average over the same window
    def setBollingerMidBand(self, window):
        if 'sma'+str(window) not in self.df.columns:
            self.setAvgSma(window)
        return self

    def setBollingerHighBand(self, window):
        assert (window > 1)
        columnName = 'bollingerHighBand'+str(window)
        multiplier = 1.7 + (0.2 * log(window, 10))
        self.df[columnName] = self.df['sma'+str(window)] + (multiplier * self.df['std'+str(window)])
        return self

    def setBollingerLowBand(self, window):
        assert (window > 1)
        columnName = 'bollingerLowBand'+str(window)
        multiplier = 1.7 + (0.2 * log(window, 10))
        self.df[columnName] = self.df['sma'+str(window)] - (multiplier * self.df['std'+str(window)])
        return self
        
    def setHighBand(self, window):
        self.df['highBand'+str(window)] = self.df['high'].rolling(window=window, min_periods=1).max()
        return self
        
    def setLowBand(self, window):
        self.df['lowBand'+str(window)] = self.df['low'].rolling(window=window, min_periods=1).min()
        return self

    def setPercentExtreme(self, window):
        # range: [-1, 1]
        columnName = 'percent_extreme'+str(window)
        self.df[columnName] = -1 + 2*(
                (self.df['avg'] - self.df['bollingerLowBand'+str(window)])
                / (self.df['bollingerHighBand'+str(window)] - self.df['bollingerLowBand'+str(window)]))
        self._addFeature(columnName)
        return self
        
    # cmfv: cumulative money flow volume
    def setCmfv(self, window):
        self.df['cmfv'+str(window)] = self.df['mfv'].rolling(window=window, min_periods=1).sum()
        return self
        
    def setPercentCmfv(self, window):
        columnName = 'percent_cmfv'+str(window)
        self.df[columnName] = self.df['cmfv'+str(window)]/self.df['sma'+str(window)]
        self._addFeature(columnName)
        return self
    
    # high/low[#] simple moving average
    def setHighSma(self, highWindow, smaWindow):
        self.df['highBand'+str(highWindow)+'Sma'+str(smaWindow)] = (
            self.df['highBand'+str(highWindow)].rolling(window=smaWindow, min_periods=1).mean())
        return self

    def setLowSma(self, lowWindow, smaWindow):
        self.df['lowBand'+str(lowWindow)+'Sma'+str(smaWindow)] = (
            self.df['lowBand'+str(lowWindow)].rolling(window=smaWindow, min_periods=1).mean())
        return self

    # new high/low[#] simple moving average
    def setNewHighSma(self, highWindow, smaWindow, volumeEma, volumeSma):
        self.df['newHigh'+str(highWindow)+'Sma'+str(smaWindow)] = (
            (self.df['avg'] - self.df['highBand'+str(highWindow)+'Sma'+str(smaWindow)])
            * self.df['percent_volumeE'+str(volumeEma)+'-'+str(volumeSma)] 
            / self.df['highBand'+str(highWindow)+'Sma'+str(smaWindow)])
        return self
        
    # rsi: relative strength index measures % overbought/oversold in a sideways market
    def setRsi(self, window):
        columnName = 'rsi'+str(window)
        self.df[columnName] = (
            100 - 100 / (1 + (self.df['posChange'].rolling(window=window, min_periods=1).mean()
                              / self.df['negChange'].rolling(window=window, min_periods=1).mean())))
        self._addFeature(columnName)
        return self

    def setCommodityChannelIndex(self, window):
        # helps find the start and end of trends
        columnName = 'cci'+str(window)
        Difference = (self.df['tp'] - self.df['tpSma'+str(window)])
        AbsoluteDifference = Difference.abs()
        MeanDeviation = AbsoluteDifference.rolling(window=window, min_periods=1).sum() / window
        self.df[columnName] = Difference / (0.015 * MeanDeviation + EPSILON)
        self._addFeature(columnName)
        return self

    def setAvgTrueRange(self, window):
        columnName = 'avgTrueRange'+str(window)
        self.df['highToPrevCloseAbsDiff'] = (self.df['high'] - self.df['prev_close']).abs()
        self.df['lowToPrevCloseAbsDiff'] = (self.df['low'] - self.df['prev_close']).abs()
        self.df['trueRange'] = self.df[['range', 'highToPrevCloseAbsDiff', 'lowToPrevCloseAbsDiff']].max(axis=1)
        self.df[columnName] = self.df['trueRange'].rolling(window=window, min_periods=1).mean()
        self._addFeature(columnName)
        return self

    def sigmoidActivation(self, columnName, steepness=1):
        sigmoidColumnName = 'sigmoid_'+str(columnName)
        self.df[sigmoidColumnName] = self.df[columnName]
        self.df[sigmoidColumnName] = (2 / (1 + E**(-steepness * self.df[sigmoidColumnName]))) - 1
        self._addFeature(sigmoidColumnName)
        return self
        
    def setFeatureColumns(self):
        
        for val in self.timePeriods:
            self.setVolumeSma(val)
            self.setVolumeEma(val)
            self.setTpSma(val)
            self.setAvgSma(val)
            self.setAvgEma(val)
            self.setAvgVariance(val)
            self.setVolumeWeightedPercentSma(val)
            self.setCommodityChannelIndex(val)
            self.setAvgTrueRange(val)
            self.setMomentum(val)
            self.setPriceRateOfChange(val)
            self._setFuturePriceChange(days=val)
            self.setStandardDeviation(val)
            self.setBollingerHighBand(val)
            self.setBollingerMidBand(val)
            self.setBollingerLowBand(val)
            self.sigmoidActivation('momentum'+str(val), steepness=1)

        self.setMovingAverageRatio(7, 14)
        self.setMovingAverageRatio(14, 21)
        self.setMovingAverageRatio(21, 50)
        self.setMovingAverageRatio(50, 100)
        self.setMovingAverageRatio(100, 200)

        self.setMomentumRatio(21, 50)
        self.setMomentumRatio(50, 100)
        self.setMomentumRatio(100, 200)

        self.setPercentVolumeEmaSmaMacd(14, 21)
        self.setPercentVolumeEmaSmaMacd(14, 50)
        self.setPercentVolumeEmaSmaMacd(14, 100)
        self.setPercentVolumeEmaSmaMacd(14, 200)

        self.setPercentVolumeSmaMacd(14, 50)
        self.setPercentVolumeSmaMacd(14, 200)
        self.setPercentVolumeSmaMacd(21, 50)
        self.setPercentVolumeSmaMacd(50, 100)
        self.setPercentVolumeSmaMacd(50, 200)

        self.setMacd(14, 21)
        self.setMacd(14, 50)
        self.setMacd(21, 50)
        self.setMacd(50, 100)
        self.setMacd(50, 200)

        self.setPercentMacd(14, 21)
        self.setPercentMacd(14, 50)
        self.setPercentMacd(21, 50)
        self.setPercentMacd(50, 100)
        self.setPercentMacd(50, 200)

        # self.df['volumeWeightedEma'] = self.df['percentVolume'] * self.df['ema50']

        # mfv: money flow volume
        self.df['mfv'] = (self.df['close'] - self.df['open'])*self.df['percent_volumeE14-200']

        for val in self.timePeriods:
            self.setCmfv(val)
            self.setPercentCmfv(val)
            self.setRsi(val)
        # self.df['cmfv400Sma200'] = self.df.cmfv400.rolling(window=200).mean()

        self.df['percent_cmfv21Sma21'] = self.df.percent_cmfv21.rolling(window=21, min_periods=1).mean()

        # high/low[#]
        for index, val in enumerate(self.timePeriods):
            self.setHighBand(val)
            self.setLowBand(val)
            self.setStochasticMomentumIndex(val)
            self.setPercentExtreme(val)
            self.setPercentCDailyChange(val)
            for innerVal in filter(lambda x: x <= self.timePeriods[index], self.timePeriods):
                self._setFuturePercentExtreme(val, days=innerVal)
                self._setFutureCategoryExtreme(val, days=innerVal)
                self._setFuturePercentCDailyChange(val, days=innerVal)
            self._setFutureCategoryPercentCDailyChange(val)

        # high/low[#] simple moving average
        self.setHighSma(14, 5)
        self.setHighSma(21, 5)
        self.setHighSma(50, 14)
        self.setHighSma(100, 21)
        self.setHighSma(200, 21)
        
        self.setLowSma(14, 5)
        self.setLowSma(21, 5)
        self.setLowSma(50, 14)
        self.setLowSma(100, 21)
        self.setLowSma(200, 21)

        # new high/low[#] simple moving average
        self.df['newHigh14Sma5'] = ((self.df['avg'] - self.df['highBand14Sma5'])
                                    * self.df['percent_volumeE14-50'] / self.df['highBand14Sma5'])

        self.df['newHigh21Sma5'] = ((self.df['avg'] - self.df['highBand21Sma5'])
                                    * self.df['percent_volumeE14-50'] / self.df['highBand21Sma5'])

        self.df['newHigh50Sma14'] = ((self.df['avg'] - self.df['highBand50Sma14'])
                                     * self.df['percent_volume50-100'] / self.df['highBand50Sma14'])

        self.df['newHigh100Sma21'] = ((self.df['avg'] - self.df['highBand100Sma21'])
                                      * self.df['percent_volume50-200'] / self.df['highBand100Sma21'])

        self.df['newHigh200Sma21'] = ((self.df['avg'] - self.df['highBand200Sma21'])
                                      * self.df['percent_volume50-200'] / self.df['highBand200Sma21'])

        self.df['newLow14Sma5'] = ((self.df['avg'] - self.df['lowBand14Sma5'])
                                   * self.df['percent_volumeE14-50'] / self.df['lowBand14Sma5'])

        self.df['newLow21Sma5'] = ((self.df['avg'] - self.df['lowBand21Sma5'])
                                   * self.df['percent_volumeE14-50'] / self.df['lowBand21Sma5'])

        self.df['newLow50Sma14'] = ((self.df['avg'] - self.df['lowBand50Sma14'])
                                    * self.df['percent_volume50-100'] / self.df['lowBand50Sma14'])

        self.df['newLow100Sma21'] = ((self.df['avg'] - self.df['lowBand100Sma21'])
                                     * self.df['percent_volume50-200'] / self.df['lowBand100Sma21'])

        self.df['newLow200Sma21'] = ((self.df['avg'] - self.df['lowBand200Sma21'])
                                     * self.df['percent_volume50-200'] / self.df['lowBand200Sma21'])

        # smoothed new high/low[#] simple moving average
        # self.df['newHigh100Sma21_sma14'] = self.df.newHigh100Sma21.rolling(window=14).mean()

        # trends - new highs/lows supported by percent volume
        self.df['trend14'] = self.df['newHigh14Sma5'] - self.df['newLow14Sma5']
        self.df['trend21'] = self.df['newHigh21Sma5'] - self.df['newLow21Sma5']
        self.df['trend50'] = self.df['newHigh50Sma14'] - self.df['newLow50Sma14']
        self.df['trend100'] = self.df['newHigh100Sma21'] - self.df['newLow100Sma21']
        self.df['trend200'] = self.df['newHigh200Sma21'] - self.df['newLow200Sma21']
        self._addFeature('trend14')
        self._addFeature('trend21')
        self._addFeature('trend50')
        self._addFeature('trend100')
        self._addFeature('trend200')

        self.df['trend14Sma14'] = self.df.trend14.rolling(window=14, min_periods=1).mean()
        self.df['trend21Sma21'] = self.df.trend21.rolling(window=21, min_periods=1).mean()
        self.df['trend50Sma50'] = self.df.trend50.rolling(window=50, min_periods=1).mean()
        self.df['trend100Sma100'] = self.df.trend100.rolling(window=100, min_periods=1).mean()
        self._addFeature('trend14Sma14')
        self._addFeature('trend21Sma21')
        self._addFeature('trend50Sma50')
        self._addFeature('trend100Sma100')
        # Make trends smoother

        print(len(self.features), "features")
        print(self.features)

    def _setFuturePriceChange(self, days=0):
        columnName = 'future_priceChange'+str(days)
        self.df[columnName] = ((self.df['avg'].shift(-days) / self.df['avg']) - 1) * 100
        self.df.fillna(0, inplace=True)
        self._addFutureFeature(columnName)

    def _setFuturePercentCDailyChange(self, window, days=0):
        columnName = 'future_percent_cdailyChange'+str(window)
        self.df[columnName] = (
            (self.df['percent_cdailyChange'+str(window)]).shift(-days))
        self.df.fillna(0, inplace=True)
        self._addFutureFeature(columnName)

    def _setFutureCategoryPercentCDailyChange(self, window):
        # values 1, 0, -1
        columnName = 'future_category_percent_cdailyChange' + str(window)
        futurePercentCDailyChange = 'future_percent_cdailyChange'+str(window)
        self.df[columnName] = 0
        self.df.loc[self.df[futurePercentCDailyChange] > 0.02, columnName] = 1
        self.df.loc[self.df[futurePercentCDailyChange] < 0.02, columnName] = -1
        self._addFutureFeature(columnName)

    def _setFuturePercentExtreme(self, window, days=0):
        columnName = 'future_percent_extreme'+str(window)+"-"+str(days)
        self.df[columnName] = (
            (self.df['percent_extreme'+str(window)]).shift(-days))
        self.df.fillna(0, inplace=True)
        self._addFutureFeature(columnName)

    def _setFutureCategoryExtreme(self, window, days=0):
        # values 1, 0, -1
        columnName = 'future_categoryExtreme' + str(window)
        futurePercentExtreme = 'future_percent_extreme'+str(window)+"-"+str(days)
        self.df[columnName] = 0
        self.df.loc[self.df[futurePercentExtreme] > 0.5, columnName] = 1
        self.df.loc[self.df[futurePercentExtreme] < -0.5, columnName] = -1
        self._addFutureFeature(columnName)

    def _setFutureCategoryHigh(self, window, days=0):
        # values 1, 0
        columnName = 'future_categoryHigh' + str(window)
        futurePercentExtreme = 'future_percent_extreme'+str(window)+"-"+str(days)
        self.df[columnName] = 0
        self.df.loc[self.df[futurePercentExtreme] > 0.5, columnName] = 1

    def _setFutureCategoryLow(self, window, days=0):
        # values 0, -1
        columnName = 'future_categoryLow' + str(window)
        futurePercentExtreme = 'future_percent_extreme'+str(window)+"-"+str(days)
        self.df[columnName] = 0
        self.df.loc[self.df[futurePercentExtreme] < -0.5, columnName] = -1

    def _addFeature(self, columnName):
        assert(columnName in self.df), "Error: dataframe must contain column: "+columnName
        self.features.append(columnName)

    def _addFutureFeature(self, columnName):
        assert(columnName in self.df), "Error: dataframe must contain column: "+columnName
        self.futureFeatures.append(columnName)

    def _setDefaultColumns(self):
        self.df['avg'] = (self.df['open']+self.df['close']+self.df['high']+self.df['low'])/4
        self.df['tp'] = (self.df['close']+self.df['high']+self.df['low'])/3     # typical price
        self.df['prev_avg'] = self.df['avg'].shift(1)
        self.df['prev_open'] = self.df['open'].shift(1)
        self.df['prev_close'] = self.df['close'].shift(1)
        self.df['prev_high'] = self.df['high'].shift(1)
        self.df['prev_low'] = self.df['low'].shift(1)

        # change
        self.df['change'] = self.df['close'] - self.df['open']
        self.df['percent_change'] = self.df['change']/self.df['avg']
        self.df['dailyChange'] = self.df['avg'] - self.df['prev_avg']
        self.df['percent_dailyChange'] = self.df['dailyChange']/self.df['prev_avg']

        # range
        self.df['range'] = self.df['high'] - self.df['low']
        self.df['percent_range'] = self.df['range']/self.df['avg']

        # ratios
        self.df['close/open'] = (self.df['close'] / self.df['open']) - 1
        self.df['high/low'] = (self.df['high'] / self.df['low']) - 1

        # pos/neg change
        self.df['posChange'] = self.df['change'].mask(self.df['change'] < 0, 0)
        self.df['negChange'] = - (self.df['change'].mask(self.df['change'] > 0, 0))
