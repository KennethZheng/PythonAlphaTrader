from backtest.BackTestFramework import BackTestFramework
from tradingplan import *


def runCustomBackTestNYSE(NYSE, tradingPlan):
    """
    Run a custom backtest on the NYSE. A rolling backtest on the SP500 for the specified trading plan is performed and
    the results are compared to the benchmark Buy and Hold strategy.

    :param NYSE:
    :return:
    """

    # create backtest framework instance for the user specified trading plan
    backTestFramework = BackTestFramework(NYSE)
    backTestFramework.setInvestorProfile(10000, 0, 5)
    backTestFramework.setTradingPlan(tradingPlan)

    # create backtest framework instance for the benchmark Buy and Hold strategy
    backTestFramework2 = BackTestFramework(NYSE)
    backTestFramework2.setInvestorProfile(10000, 0, 5)
    backTestFramework2.setTradingPlan(BuyAndHold())

    # perform rolling backtest for both strategies
    backTestResult1 = backTestFramework.rollingBackTest(windowSizeRatio=0.4, backTestRatio=0.25, incrementDays=400)
    backTestResult2 = backTestFramework2.rollingBackTest(windowSizeRatio=0.4, backTestRatio=0.25, incrementDays=400)
    print("average CAGR of rolling backtest1:", backTestResult1.getAverageAnnualReturn())
    print("average CAGR of rolling backtest2 (Buy and Hold):", backTestResult2.getAverageAnnualReturn())

    # plot net worth comparision for each backtest in the rolling backtest
    for index, backtest1 in enumerate(backTestResult1.backTests, start=0):
        investor1 = backtest1.investor
        investor2 = backTestResult2.backTests[index].investor

        plt.figure(figsize=(150, 10))
        plt.title("Rolling Window Backtest comparison of Selected Trading Plan over Buy and Hold")
        plt.plot(investor1.portfolio.history.index, investor1.portfolio.history[['netWorth', 'units']])
        plt.plot(investor2.portfolio.history.index, investor2.portfolio.history[['netWorth', 'units']])
        plt.show()
