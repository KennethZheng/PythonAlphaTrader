from util import Constant as const


class Investor:
    """
    The investor is used during a backtest to track a portfolio of stocks or cash at any given date. A trading plan
    sends orders to the stock market through the interface provided by the investor class.
    """

    def __init__(self, cash, savingsInterest, tradingFee, stockExchange, tradingPlan):
        self.dateStart = None
        self.date = None

        self.startNetWorth = cash
        self.netWorth = self.startNetWorth
        self.tradingFee = tradingFee
        self.tradingPlan = tradingPlan
        self.stockExchange = stockExchange
        self.savingsInterest = savingsInterest
        self.dayOfMonth = 0     # counter for when to pay accrued savings account interest
        self.portfolio = Portfolio(self, self.startNetWorth, self.savingsInterest, self.stockExchange)

        self.unitsBought = 0
        self.buys = 0
        self.failedBuys = 0
        self.unitsSold = 0
        self.sells = 0
        self.failedSells = 0
        self.maxNetWorth = self.netWorth

        self.marketPrices = [0] * self.stockExchange.stockCount
        self.marketCloses = [0] * self.stockExchange.stockCount
        self.marketHighs = [0] * self.stockExchange.stockCount
        self.marketLows = [0] * self.stockExchange.stockCount

    def initTradingPlan(self, earliestDate, dateStart, latestDate):
        return self.tradingPlan.initPlan(self, earliestDate, dateStart, latestDate)
    
    def plan(self):
        self.tradingPlan.plan(self)
    
    def buy(self, units):
        if units == 0:
            return
        _tickerSymbol = self.stockExchange.getStockByIndex(0).tickerSymbol
        if self.portfolio.cash >= units*self.marketCloses[0]:   # commission free ETFs
            print("\ndate", self.date, "\tBuying", units, "units at $", self.marketCloses[0])
            # print("net worth: $", self.netWorth)
            # print("Buying", units, "units at $", self.marketCloses[0])

            cashOnHold = units*self.marketCloses[0]
            self.portfolio.changeCash(-cashOnHold, self.date)
            self.stockExchange.sendBuyOrder(self, _tickerSymbol, self.date, units, self.marketCloses[0], cashOnHold)
            return 0
        else:
            print("ERROR: Failed to buy")
            return 1

    def midPointBuy(self, cash, valueHolder=None):
        if cash <= 0:
            return
        _tickerSymbol = self.stockExchange.getStockByIndex(0).tickerSymbol
        if self.portfolio.cash >= cash:   # commission free ETFs
            print("\ndate", self.date, "\tBuying at mid-point with $", cash)
            self.portfolio.changeCash(-cash, self.date)
            self.stockExchange.sendMidPointBuyOrder(self, _tickerSymbol, self.date, cash, valueHolder=valueHolder)
            return 0
        else:
            print("ERROR: Failed to buy")
            return 1
        
    def setStopLoss(self, units, limitPrice):
        if units == 0:
            return
        _tickerSymbol = self.stockExchange.getStockByIndex(0).tickerSymbol
        if self.portfolio.availableUnitsList[0] >= units:
            print("\ndate", self.date, "\tset stop loss", units, "units at $", limitPrice)
            # print("net worth: $", self.netWorth)
            # print("Selling", units, "units at $", self.marketCloses[0])
            # print("Trading fee ", self.tradingFee)

            self.portfolio.changeCash(-self.tradingFee, self.date)
            self.portfolio.changeAvailableUnits(0, -units, self.date)
            self.stockExchange.sendStopLossOrder(self, _tickerSymbol, self.date, units, limitPrice, self.tradingFee)
            return 0
        else:
            print("ERROR: Failed to set stop loss")
            return 1
        
    def sell(self, units, limitPrice=None):
        if units == 0:
            return
        if limitPrice is None:
            limitPrice = self.marketCloses[0]
        _tickerSymbol = self.stockExchange.getStockByIndex(0).tickerSymbol
        if self.portfolio.availableUnitsList[0] >= units:
            print("\ndate", self.date, "\tSelling", units, "units at $", limitPrice)
            # print("net worth: $", self.netWorth)
            # print("Selling", units, "units at $", self.marketCloses[0])
            # print("Trading fee ", self.tradingFee)

            self.portfolio.changeCash(-self.tradingFee, self.date)
            self.portfolio.changeAvailableUnits(0, -units, self.date)
            self.stockExchange.sendSellOrder(self, _tickerSymbol, self.date, units, limitPrice, self.tradingFee)
            return 0
        else:
            print("ERROR: Failed to sell")
            return 1
        
    def maxBuyUnits(self):
        return int(self.portfolio.cash/(self.marketCloses[0]))
    
    def averageBuyUnits(self):
        _maxBuyUnits = int(self.portfolio.cash/(self.marketCloses[0]))
        _attemptBuyUnits = (0.2*(self.portfolio.unitsList[0]+_maxBuyUnits))
        int_attemptBuyUnits = int(_attemptBuyUnits)
        if (int_attemptBuyUnits > _maxBuyUnits) | (int_attemptBuyUnits == 0):
            return _maxBuyUnits
        else:
            return int_attemptBuyUnits

    def averageBuyCash(self):
        return self.portfolio.cash * 0.2

    def maxBuyCash(self):
        return self.portfolio.cash
    
    def printNetWorth(self):
        print("Cash $", self.portfolio.cash)
        # print("Units owned: ", self.portfolio.unitsList[0], " with total value $",
        # self.marketPrices[0]*self.portfolio.unitsList[0])
        avg_annual_return = self.getAnnualReturn()
        # print("Average annual return ",avg_annual_return)
        print("Average annual return:\n % 6.3f" % ((avg_annual_return-1)*100), "%")
        print("\tNet worth of $%9.2f" % self.netWorth)
        
    def getAnnualReturn(self):
        # remove dependency on backtest to set dateStart
        assert(self.dateStart is not None)
        avg_annual_return = 0
        if self.date > self.dateStart:
            years = (self.date-self.dateStart+1)/const.YEAR
            avg_annual_return = (self.netWorth/self.startNetWorth)**(1/years)
        return avg_annual_return
        
    # update info
    def updateMarketPrices(self):   # update the investors market price
        for i in range(self.stockExchange.stockCount):
            self.marketPrices[i] = self.stockExchange.getStockByIndex(i).df.loc[self.date]['avg']
            self.marketCloses[i] = self.stockExchange.getStockByIndex(i).df.loc[self.date]['close']
            self.marketHighs[i] = self.stockExchange.getStockByIndex(i).df.loc[self.date]['high']
            self.marketLows[i] = self.stockExchange.getStockByIndex(i).df.loc[self.date]['low']
    
    def updateNetWorth(self):
        self.netWorth = self.portfolio.cash
        for i in range(self.stockExchange.stockCount):
            self.netWorth += self.marketPrices[i]*self.portfolio.unitsList[0]
        self.portfolio.updateNetWorth(self.netWorth, self.date)
    
    def applySavingsInterest(self):
        self.dayOfMonth += 1
        monthlyInterest = 0
        if self.portfolio.cash * (1+self.savingsInterest/const.YEAR) >= 0.01:
            monthlyInterest += (self.portfolio.cash * self.savingsInterest/const.YEAR)
        if (self.dayOfMonth == int(const.YEAR/12)) & (monthlyInterest > 0):
            print("Savings Monthly Interest $%9.2f" % monthlyInterest)
            self.portfolio.cash += monthlyInterest
            self.dayOfMonth = 0
        
    def setDate(self, date):
        self.date = date
        self.updateMarketPrices()
        self.applySavingsInterest()
        self.updateNetWorth()
        # debug
        print("inv date", date, "\t$", self.stockExchange.getStockByIndex(0).df.loc[date]['avg'])


class Portfolio:
    """
    A portfolio tracks a collection of stocks and cash of a particular investor over time.
    """
    
    def _initHistory(self):
        self.history = self.stockExchange.getStockByIndex(0).df[['date']].copy(deep=True)
#         self.history['cash'] = self.cash
        self.history['units'] = self.unitsList[0]
        self.history['netWorth'] = 0
        
    def __init__(self, investor, cash, savingsInterest, stockExchange):
        self.investor = investor
        self.cash = cash
        self.savingsInterest = savingsInterest
        self.stockExchange = stockExchange
        self.unitsList = [0] * self.stockExchange.stockCount
        self.availableUnitsList = [0] * self.stockExchange.stockCount
        self._initHistory()
        
    def changeCash(self, _cash, _date):
        self.cash += _cash
        # (self.history).loc[_date:,'cash'] = self.cash

    @property
    def cash(self):
        # return dollar value as float with 2 decimal precision
        # to do: override arithmetic operators to reduce numerical error by using integer representation
        return float(self.__cash) / 100

    @cash.setter
    def cash(self, _cash):
        # sets cash in cents of type int
        self.__cash = int(_cash * 100)
        
    def changeUnits(self, _index, _units, _date):
        self.unitsList[_index] += _units
        self.history.loc[_date:, 'units'] = self.unitsList[_index]*500
        
    def changeAvailableUnits(self, _index, _units, _date):
        self.availableUnitsList[_index] += _units
        
    def updateNetWorth(self, _netWorth, _date):
        self.history.loc[_date:, 'netWorth'] = _netWorth
