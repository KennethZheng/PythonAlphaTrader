import time
import backtest.Investor as inv
import matplotlib.pyplot as plt


class BackTestFramework:
    """
    Provides the backtesting environment for a specific stock market universe and investor profile.
    """

    def __init__(self, stockExchange):
        self.stockExchange = stockExchange
        self.latestDate = self.stockExchange.lastIndex
        self.earliestDate = 0
        self.investorProfile = None
        self.tradingPlan = None
        self.investor = None
        
    def setInvestorProfile(self, cash, savingsInterest, tradingFee):
        assert(self.investorProfile is None)
        self.investorProfile = dict()
        self.investorProfile['cash'] = cash
        self.investorProfile['savingsInterest'] = savingsInterest
        self.investorProfile['tradingFee'] = tradingFee
        
    def setTradingPlan(self, tradingPlan):
        self.tradingPlan = tradingPlan
    
    # backtest the trading plan
    def _backTest(self, earliestDate, latestDate, backTestRatio):
        assert (backTestRatio > 0.1)
        assert (backTestRatio < 0.9)
        assert(self.investorProfile is not None)
        assert(self.tradingPlan is not None)

        backTestInstance = BackTest(
            self.investorProfile, self.tradingPlan, earliestDate, latestDate, backTestRatio, self.stockExchange)
        backTestInstance.execute()

        return backTestInstance

    def singleBackTest(self, backTestRatio):
        return self._backTest(self.earliestDate, self.latestDate, backTestRatio)

    def rollingBackTest(self, windowSizeRatio, backTestRatio, incrementDays):
        assert(windowSizeRatio > 0.1)
        assert(windowSizeRatio < 0.9)
        assert(self.earliestDate == 0)
        assert(incrementDays < self.latestDate)
        # assert earliestDate always starts at 0 for window calculation
        window = int(windowSizeRatio * self.latestDate)
        backTests = RollingBackTest()
        for earliestSubDate in range(self.earliestDate, self.latestDate, incrementDays):
            latestSubDate = earliestSubDate + window
            if latestSubDate <= self.latestDate:
                backTestInstance = self._backTest(earliestSubDate, latestSubDate, backTestRatio)
                backTests.append(backTestInstance)

        return backTests


class BackTest:

    def __init__(self, investorProfile, tradingPlan, earliestDate, latestDate, backTestRatio, stockExchange):
        self.earliestDate = earliestDate
        self.latestDate = latestDate
        self.backTestRatio = backTestRatio
        self.dateStart = latestDate - round(backTestRatio * (latestDate - earliestDate))
        self.dateEnd = latestDate
        self._averageAnnualReturn = None
        self.investorProfile = investorProfile
        self.stockExchange = stockExchange
        self.tradingPlan = tradingPlan
        self._executed = False

        self.investor = inv.Investor(
            self.investorProfile['cash'],
            self.investorProfile['savingsInterest'],
            self.investorProfile['tradingFee'],
            self.stockExchange,
            self.tradingPlan)

        self.investor.initTradingPlan(self.earliestDate, self.dateStart, self.dateEnd)

    def execute(self):
        self._validateDateRange()
        self.investor.dateStart = self.dateStart

        startTime = time.time()
        for date in range(self.dateStart, self.dateEnd+1):
            self.stockExchange.setDate(date)
            self.investor.setDate(date)
            if date != self.dateEnd:
                self.investor.plan()

        self._printBackTestResults(startTime)
        self._averageAnnualReturn = self.investor.getAnnualReturn()
        self._executed = True

    @property
    def averageAnnualReturn(self):
        assert(self._averageAnnualReturn is not None)
        assert(self._executed is True)
        return self._averageAnnualReturn

    def _validateDateRange(self):
        assert (self.dateStart is not None)
        assert (self.dateEnd is not None)
        assert (self.dateStart < self.dateEnd)
        assert (self.dateStart >= self.earliestDate)
        assert (self.dateEnd <= self.latestDate), (
                "Error: dateEnd = "+str(self.dateEnd)+"is greater than latestDate = "+str(self.latestDate))

    def _printBackTestResults(self, startTime):
        print("=====================")
        print("Backtest complete", )
        print("=====================")
        print("Finished in %3.2f" % (time.time()-startTime), "sec")
        self.investor.updateNetWorth()
        self.investor.printNetWorth()
        print("Total units bought ", self.investor.unitsBought)
        print("Total units sold ", self.investor.unitsSold)
        print("Total buys ", self.investor.buys)
        print("Total sells ", self.investor.sells)
        print("Total failed buys ", self.investor.failedBuys)
        print("Total failed sells ", self.investor.failedSells)

        # plot net worth
        plt.figure(figsize=(150, 10))
        plt.title("Backtest Graph of Net Worth over Time")
        plt.plot(self.investor.portfolio.history.index, self.investor.portfolio.history[['netWorth', 'units']])
        plt.show()

# create a backtest result class which implements a writable/serializable class for I/O


class RollingBackTest:

    def __init__(self):
        self.backTests = list()

    # create a method to get aggregate result classes

    def append(self, backTest):
        assert(type(backTest) is BackTest)
        self.backTests.append(backTest)

    def getAverageAnnualReturn(self):
        sumOfAverageAnnualReturns = 0
        for backTest in self.backTests:
            sumOfAverageAnnualReturns += backTest.averageAnnualReturn

        return float(sumOfAverageAnnualReturns) / len(self.backTests)
